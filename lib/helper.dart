// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/tuto_asigna.dart';
import 'package:tc_app/tuto_basic.dart';
import 'package:tc_app/tuto_inicio.dart';
import 'package:tc_app/tuto_vehiculos.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http_parser/http_parser.dart';
import 'dart:math';
import 'inicio_dashboard.dart';
import 'login.dart';
import 'tools/constants.dart';

class Helper {
  static void showNewDialog(String titulo, String mensaje, context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static Future<http.Response> getSWData(String url,
      {required Map bodySend}) async {
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {"Content-type": "application/x-www-form-urlencoded"},
          body: bodySend);
    } catch (e) {
      return http.Response('{"message":"Network Error"}', 500);
    }
    return response;
  }

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<bool> savePreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  static terminarTuto(context) {
    var res = Helper.actualizarBoolUser("tutorial", false);
    if (res != null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => InicioDashboard()),
      );
    }
  }

  static linkTuto(paso, context) {
    switch (paso) {
      case "":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoInicio()),
        );
        break;
      case "1":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoInicio()),
        );
        break;
      case "2":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoAsigna()),
        );
        break;
      case "3":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoBasic()),
        );
        break;
      case "4":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoVehiculos()),
        );
        break;
      default:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoInicio()),
        );
        break;
    }
  }

  static actualizarPaso(paso) async {
    var id = await Helper.getPreferences('userid');
    var token = await Helper.getPreferences('jwt');
    var ruta = "${server}users";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"paso": paso};

    var response = await http.put(Uri.parse(ruta + "/" + id!),
        body: json.encode(values), headers: headers);

    if (response.statusCode == 200) {
      await Helper.savePreferences('paso', paso);
    } else {
      // onSubmitted("Error!, El Usuario no se ha actualizado");

      print("no se ha acualizado el paso");
    }
  }

  static Future<bool> actualizarBoolUser(String campo, bool valor) async {
    var id = await Helper.getPreferences('userid');
    var token = await Helper.getPreferences('jwt');
    var ruta = "${server}users";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {campo: valor.toString()};

    var response = await http.put(Uri.parse(ruta + "/" + id!),
        body: json.encode(values), headers: headers);

    if (response.statusCode == 200) {
      await Helper.savePreferencesBool(campo, valor);
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> savePreferencesBool(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
    return true;
  }

  static Future<bool?> getBoolPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var regreso = prefs.getBool(key);
    return regreso;
  }

  static void navigateUser(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  static void launchURL(lat, long) async {
    var url =
        'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + long;
    // if (await canLaunch(url)) {
    //   await launch(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
    if (!await launchUrl(
      Uri.parse(url),
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  static uploadCamera() async {
    ImagePicker picker = ImagePicker();
    XFile? image =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    return image;
  }

  static uploadDevice() async {
    ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: ImageSource.gallery);

    return image;
  }

  static createSession(responseData) async {
    await Helper.savePreferences("jwt", responseData['jwt']);
    await Helper.savePreferences(
        "userid", responseData['user']['id'].toString());
    await Helper.savePreferences("username", responseData['user']['username']);
    await Helper.savePreferences("email", responseData['user']['email']);
    await Helper.savePreferences(
        "roleid", responseData['user']['role']['id'].toString());
    await Helper.savePreferences(
        "nivel", responseData['user']['nivel']['id'].toString());
    await Helper.savePreferences(
        "precio", responseData['user']['price'].toString());
    await Helper.savePreferences(
        "rolename", responseData['user']['role']['name']);
    await Helper.savePreferencesBool('isLoggedIn', true);
    await Helper.savePreferencesBool(
        'aviso',
        responseData['user']['aviso'] == null
            ? false
            : responseData['user']['aviso']);
  }

  static String getRandomString(int length) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(
        length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  static cargarImagen(image, rutaUpload, id, ref, field, token) async {
    var name = "image" + Helper.getRandomString(15);

    // print("------Inicio Metodo----------");
    // print(image);
    // print(rutaUpload);
    // print(id);
    // print(ref);
    // print(field);
    // print(token);
    // print("-------fin de metodo----------");

    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token'
    };
    var uri = Uri.parse(rutaUpload);
    var request = http.MultipartRequest('POST', uri)
      ..fields['ref'] = ref
      ..fields['refId'] = id.toString()
      ..fields['field'] = field
      ..files.add(new http.MultipartFile.fromBytes(
          'files', image.readAsBytesSync(),
          filename: name + ".${image.path.split(".").last}",
          contentType: MediaType("image", "${image.path.split(".").last}")));

    request.headers.addAll(headers);
    var response = await request.send();
    // print(response.statusCode);

    if (response.statusCode == 200) {
      print('Uploaded!');
      print("true");
      return true;
    } else {
      print("false");
      return false;
    }
  }
}
