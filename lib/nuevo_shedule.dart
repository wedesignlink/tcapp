// import 'package:donchonito_app/recibido.dart';

// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/shedule.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as search;
import 'funciones.dart';
import 'helper.dart';
import 'login.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevoShedule extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevoShedule> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController dateCtl = TextEditingController();
  String proyectos = "${server}proyectos";
  String trabajadores = "${server}users";
  String ruta = "${server}shedules";
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  // late search.SearchBar searchBar;
  List dataProyectos = [];
  List dataUsers = [];
  TextEditingController descController = TextEditingController();
  late String? _dropdownValueProyecto;
  late String? _dropdownValueUser;

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void getDataProyectos(ruta) async {
    var token = await getPreferences('jwt');
    var company = await getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?user.company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    // print(resBody);

    setState(() {
      dataProyectos = resBody;
    });
  }

  void getDataTrabajadores(ruta) async {
    var token = await getPreferences('jwt');
    var company = await getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(
        Uri.parse(ruta + "?role.id=2&company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    print(resBody);

    setState(() {
      dataUsers = resBody;
    });
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != finicio)
      setState(() {
        finicio = pickedDate;
        // String finicio =
        //     "${pickedDate.year}/${pickedDate.month}/${pickedDate.day}";
      });
  }

  void guardarShedule(ruta) async {
    var token = await getPreferences('jwt');
    var user = await getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "proyecto": _dropdownValueProyecto!,
      "user": _dropdownValueUser!,
      "descripcion": descController.text,
      "fecha_inicio": finicio.toString(),
      "user_register": user!
    };

    print(values);

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    print(response.statusCode);
    print(responseData);

    if (response.statusCode == 200) {
      // setState(() => _loading = false);
      _showDialog('OK!', 'Se guardado el usuario correctamente');
      Helper.actualizarPaso("3");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Shedule()),
      );
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarShedule(ruta);
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    getDataProyectos(proyectos);
    getDataTrabajadores(trabajadores);
    // navigateUser();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Nueva asignación',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[
              // IconButton(
              //   icon: const Icon(Icons.share),
              //   tooltip: 'Compartir',
              //   onPressed: () {
              //     // _modalAlias();
              //     // ScaffoldMessenger.of(context).showSnackBar(
              //     //     const SnackBar(content: Text('This is a snackbar')));
              //   },
              // ),
            ],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                child: Text(
                                  'Proyecto',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                isExpanded: true,
                                validator: (value) => validateDrop(value!),
                                value: _dropdownValueProyecto,
                                hint: Text('Seleccione un proyecto'),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _dropdownValueProyecto = newValue;
                                  });
                                },
                                items: dataProyectos.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(item['titulo']),
                                    value: item['id'].toString(),
                                  );
                                }).toList(),
                              )),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Trabajador',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                isExpanded: true,
                                validator: (value) => validateDrop(value!),
                                value: _dropdownValueUser,
                                hint: Text('Seleccione un trabajador'),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _dropdownValueUser = newValue;
                                  });
                                },
                                items: dataUsers.map((item) {
                                  return new DropdownMenuItem(
                                    child: Text(
                                        item['name'] + " " + item['lastname']),
                                    value: item['id'].toString(),
                                  );
                                }).toList(),
                              )),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                // margin: EdgeInsets.all(20),
                                child: Column(
                                  children: [
                                    // SizedBox(
                                    //   height: 20.0,
                                    // ),
                                    ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.black),
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30.0),
                                          ))),
                                      onPressed: () => {_selectDate(context)},
                                      child: Text('Fecha de inicio',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Montserrat',
                                          )),
                                    ),
                                    Text(
                                        finicio.year.toString() +
                                            "/" +
                                            finicio.month.toString() +
                                            "/" +
                                            finicio.day.toString(),
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 53, 66, 74),
                                          fontSize: 16.0,
                                          // fontWeight: FontWeight.bold,
                                        )),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                controller: descController,
                                validator: (value) => validateName(value!),
                                maxLines: 4,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe una observación'),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              ElevatedButton(
                                  child: Text('Guardar asignación',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                      )),
                                  onPressed: () {
                                    _validateInputs();
                                  },
                                  style: ButtonStyle(
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.all(20)),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      )))),
                            ],
                          )))),
            ],
          ),
        ));
  }
}
