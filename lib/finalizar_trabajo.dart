// ignore_for_file: deprecated_member_use

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:full_screen_image/full_screen_image.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as seach;
import 'package:tc_app/inicio_trabajador.dart';
import 'helper.dart';
import 'funciones.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class FinalizarTrabajo extends StatefulWidget {
  final Map shedule;
  final Map trabajo;
  const FinalizarTrabajo(
      {Key? key, required this.shedule, required this.trabajo})
      : super(key: key);
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<FinalizarTrabajo> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}proyecto-detalles";
  String rutaS = "${server}shedules";
  String rutaUpload = "${server}upload";
  TextEditingController proyectoController = TextEditingController();
  TextEditingController piesController = TextEditingController();
  TextEditingController horasController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  // Enum _autoValidate = AutovalidateMode.disabled;
  // late seach.SearchBar searchBar;
  var _image;
  late List<XFile> _imageFileList;
  var imagePicker;
  var type;
  String baseUrl = "http://159.203.164.232";
  late String nivel;

  // set _imageFile(XFile value) {
  //   _imageFileList = value == null ? null : [value];
  // }

  final ImagePicker _picker = ImagePicker();

  void guardarTrabajo(ruta) async {
    var token = await Helper.getPreferences('jwt');
    // var user = await Helper.getPreferences('userid');
    var idTrabajo = widget.trabajo['id'].toString();
    var idShedule = widget.shedule['id'].toString();
    var url = ruta + "/" + idTrabajo;
    var urls = rutaS + "/" + idShedule;

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {};

    if (nivel == "2" || nivel == "1") {
      values = {
        "Pies": piesController.text,
      };
    } else if (nivel == "3") {
      values = {
        "Horas": horasController.text,
      };
    }

    // Aquí puedes manejar el caso donde `values` sigue siendo vacío
    if (values.isEmpty) {
      // Maneja el caso donde no se establecieron valores, por ejemplo, lanzar una excepción o retornar un mensaje.
      throw Exception("No se proporcionaron valores para la actualización.");
    }

    http.Response response = await http.put(Uri.parse(url),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      var cuantas = 0;
      var listaImagenes;

      // cargamos las imagenes una a una en caso de existir
      for (var i = 0; i < _imageFileList.length; i++) {
        // print(File(_imageFileList[i].path));
        // image, rutaUpload, id, ref, field, token
        Helper.cargarImagen(File(_imageFileList[i].path), rutaUpload, idTrabajo,
            'proyecto-detalle', 'imagenes_fin', token);
        cuantas++;
      }
      listaImagenes = _imageFileList.length;

      if (cuantas == listaImagenes) {
        Map<String, String> valuesS = {
          "terminado": 'true',
          // "Horas": horasController.text,
        };

        http.Response responseS = await http.put(Uri.parse(urls),
            body: json.encode(valuesS), headers: headers);
        final responseDataS = json.decode(responseS.body);

        if (responseS.statusCode == 200) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => InicioTrabajador()),
          );
        } else if (responseS.statusCode == 400) {
          _showDialog(
              'Ups!',
              'No se ha podido guardar por favor intente de nuevo:\n\n' +
                  responseDataS["error"]);
        }
      }
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  uploadDeviceMultiple() async {
    final pickedFileList = await _picker.pickMultiImage(imageQuality: 20);
    // final pickedFileList = await ImagePicker.pickMultiImage();

    setState(() {
      _imageFileList = pickedFileList;
    });
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarTrabajo(ruta);
    } else {
// //    If all data are not valid then start auto validation.
//       _showDialog('Ups!', 'Debe completar el formulario correctamente');
//       setState(() {
//         _autoValidate = true;
//       });
    }
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("Regresar"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Confirmar"),
      onPressed: () {
        _cancelarTrabajo();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirme la acción"),
      content: Text(
          "¿Desea cancelar el trabajo actual, se perdera el progreso guardado, esta accion no es reversible?"),
      actions: [
        Row(
          children: [
            SizedBox(
              width: 10,
            ),
            Expanded(child: cancelButton),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: continueButton,
            ),
            SizedBox(
              width: 10,
            ),
          ],
        )
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _cancelarTrabajo() async {
    var idTrabajo = widget.trabajo['id'].toString();
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    http.Response response =
        await http.delete(Uri.parse(ruta + "/" + idTrabajo), headers: headers);

    if (response.statusCode == 200) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => InicioTrabajador()),
      );
    }
  }

  _getNivel() async {
    var nivelP = await Helper.getPreferences('nivel');
    // print(nivel);
    setState(() {
      nivel = nivelP!;
    });
  }

  @override
  void initState() {
    super.initState();
    _getNivel();
  }

  List<Widget> _listaImagenes() {
    List<Widget> listings = [];

    if (_imageFileList.length > 0) {
      int i = 0;
      for (i = 0; i < _imageFileList.length; i++) {
        listings.add(FullScreenWidget(
          disposeLevel: DisposeLevel.High,
          child: ClipRRect(
            // borderRadius: BorderRadius.circular(16),
            child: Image.file(
              File(_imageFileList[i].path),
              fit: BoxFit.fitWidth,
            ),
          ),
        ));
      }
      return listings;
    } else {
      return [];
    }
  }

  Widget _bloqueImagen() {
    if (_image == null) {
      return Center(
          child: ElevatedButton(
        onPressed: () {
          uploadDeviceMultiple();
        },
        child: Icon(Icons.camera, color: Colors.white),
        style: ElevatedButton.styleFrom(
          shape: CircleBorder(),
          padding: EdgeInsets.all(30),
          backgroundColor:
              Colors.yellow[700], // <-- Button color // <-- Splash color
        ),
      ));
    } else {
      return Center(
          child: Image.file(
        _image,
        fit: BoxFit.fitWidth,
      ));
    }
  }

  // ignore: missing_return
  Widget _textoCantidad(nivel) {
    if (nivel == null) {
      return Container();
    } else if (nivel == "2" || nivel == "1")
      return Center(
          child: Text('Pies trabajados',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14.0,
              )));
    else if (nivel == "3") {
      return Center(
          child: Text('Horas trabajadas',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14.0,
              )));
    } else {
      return Container();
    }
  }

  // ignore: missing_return
  Widget _nivel(nivel) {
    if (nivel == null) {
      return Container();
    } else if (nivel == "2" || nivel == "1") {
      return TextFormField(
        controller: piesController,
        validator: (value) {
          validateNumber(value!);
          return null;
        },
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Escriba los Pies trabajados'),
      );
    } else if (nivel == "3") {
      return TextFormField(
        controller: horasController,
        validator: (value) {
          validateNumber(value!);
          return null;
        },
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Escriba las horas trabajadas'),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var proyecto = widget.shedule['proyecto']['titulo'];
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text('Finalizar Trabajo',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                )),
            actions: <Widget>[],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          // // ignore: deprecated_member_use
                          // autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: Text('Proyecto',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                initialValue: proyecto,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe un concepto'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              // Center(
                              //     child: Text('Pies trabajados',
                              //         style: TextStyle(
                              //           color: Colors.black,
                              //           fontSize: 14.0,
                              //         ))),
                              _textoCantidad(nivel),
                              SizedBox(
                                height: 10,
                              ),
                              _nivel(nivel),
                              SizedBox(
                                height: 40,
                              ),
                              Center(
                                  child: Text('Fotos',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              _bloqueImagen(),
                              SizedBox(
                                height: 20,
                              ),
                              Column(
                                children: _listaImagenes(),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              Container(
                                child: ElevatedButton(
                                    child: Text('Finalizar trabajo',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14.0,
                                        )),
                                    onPressed: () {
                                      _validateInputs();
                                    },
                                    style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(20)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        )))),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                padding: EdgeInsets.only(bottom: 20),
                                child: ElevatedButton(
                                    child: Text('Cancelar trabajo'),
                                    onPressed: () {
                                      // _validateInputs();
                                      showAlertDialog(context);
                                    },
                                    style: ButtonStyle(
                                        padding:
                                            WidgetStateProperty.all<EdgeInsets>(
                                                EdgeInsets.all(20)),
                                        backgroundColor:
                                            WidgetStateProperty.all<Color>(
                                                Colors.grey[200]!),
                                        shape: WidgetStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        )))),
                              )
                            ],
                          )))),
            ],
          ),
        ));
  }
}
