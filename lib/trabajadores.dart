// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/tuto_asigna.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as search;
import 'helper.dart';
import 'login.dart';
import 'nuevo_trabajador.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class Trabajadores extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<Trabajadores> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController priceController = TextEditingController();
  // late search.SearchBar searchBar;
  String ruta = "${server}users";
  List data = [];
  List dataId = [];
  bool _tuto = false;
  // ignore: unused_field
  String _rol = "";

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void getData(ruta) async {
    var token = await getPreferences('jwt');
    var company = await getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?company.id=" + company),
        headers: headers);

    var resBody = json.decode(response.body);
    print(resBody);
    var tutorial = await Helper.getBoolPreferences('tutorial') ?? false;
    setState(() {
      data = resBody;
      _tuto = tutorial;
    });
  }

  void getDataId(ruta, id) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(
        resBody[0]["name"],
        resBody[0]["lastname"],
        resBody[0]["role"]["name"],
        resBody[0]["nivel"]["nombre"],
        resBody[0]["price"].toString(),
        resBody[0]["email"],
        resBody[0]["blocked"] ? "Activar" : "Desactivar",
        resBody[0]["id"].toString());
    setState(() {
      dataId = resBody;
      _rol = resBody[0]["role"]["name"];
    });
  }

  void _actualizarPrecio(ruta, id) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"price": priceController.text};

    var response = await http.put(Uri.parse(ruta + "/" + id),
        body: json.encode(values), headers: headers);

    // ignore: unused_local_variable
    final responseData = json.decode(response.body);

    // print(response.statusCode);
    // print(responseData);

    if (response.statusCode == 200) {
      priceController.clear();
      onSubmitted("Ok, El usuario se ha actualizado");
    } else {
      onSubmitted("Error!, El Usuario no se ha actualizado");
    }
  }

  void _actualizarBloqueo(ruta, id, block) async {
    var token = await getPreferences('jwt');
    var bloqueo;
    if (block == "Activar") {
      bloqueo = "false";
    } else if (block == "Desactivar") {
      bloqueo = "true";
    }

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"blocked": bloqueo};

    var response = await http.put(Uri.parse(ruta + "/" + id),
        body: json.encode(values), headers: headers);

    // ignore: unused_local_variable
    final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      onSubmitted("Ok, El usuario se ha actualizado");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Trabajadores()),
      );
    } else {
      onSubmitted("Error!, El Usuario no se ha actualizado");
    }
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value'))));
  }

  // _MyStart() {
  //   searchBar = new search.SearchBar(
  //       inBar: false,
  //       buildDefaultAppBar: buildAppBar,
  //       setState: setState,
  //       onSubmitted: onSubmitted,
  //       onCleared: () {
  //         print("cleared");
  //       },
  //       onClosed: () {
  //         print("closed");
  //         Navigator.push(
  //           context,
  //           MaterialPageRoute(builder: (context) => Trabajadores()),
  //         );
  //       });
  // }

  @override
  void initState() {
    super.initState();
    navigateUser();
    getData(ruta);
    // if (!_aceptado) {
    //   _aviso();
    // }
  }

  Widget _detalle(role, nivel, price, email) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Center(
            child: Text(role + " - " + nivel),
          ),
          Center(
            child: Text(email),
          ),
          Divider(),
          SizedBox(
            height: 20,
          ),
          Center(
            child: Text('Precio x hora:' + price.toString()),
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            initialValue: null,
            controller: priceController,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Cambiar precio x hora'),
          ),
        ],
      ),
    );
  }

  // ignore: unused_element
  _launchURL() async {
    const url =
        'https://www.google.es/maps/place/1268-1260+California+Ave,+Reno,+NV+89509,+USA/@39.5128457,-119.8313767,19z/data=!3m1!4b1!4m13!1m7!3m6!1s0x809940ae9292a09d:0x40c5c5ce7438f787!2sReno,+NV,+USA!3b1!8m2!3d39.5296329!4d-119.8138027!3m4!1s0x809940d75d4bb85d:0x10f865751666ae10!8m2!3d39.5128446!4d-119.8308295';
    // if (await canLaunch(url)) {
    //   await launch(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
    // canLaunchUrl(Uri.parse(url)).then((bool result) {
    //   setState(() {
    //     _hasCallSupport = result;
    //   });
    // });
    if (!await launchUrl(
      Uri.parse(url),
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  void _modalDetalle(String name, String lastname, String role, String nivel,
      String price, String email, String block, String id) {
    Color colores = block == "Activar" ? Colors.red : Colors.black;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (
        BuildContext context,
      ) {
        return AlertDialog(
            title: Column(children: [
              Text(name + " " + lastname,
                  style: TextStyle(
                    color: colores,
                    fontSize: 16.0,
                    // fontWeight: FontWeight.bold,
                  )),
            ]),
            content: _detalle(role, nivel, price, email),
            actions: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    Expanded(
                      child: OutlinedButton(
                        onPressed: () {
                          // _launchURL();
                          _actualizarBloqueo(ruta, id.toString(), block);
                        },
                        child: Text(block),
                      ),
                    ),
                    SizedBox(width: 2),
                    OutlinedButton(
                      onPressed: () {
                        // _launchURL();
                        _actualizarPrecio(ruta, id.toString());
                        Navigator.pop(context);
                      },
                      child: const Text('Guardar'),
                    ),
                    SizedBox(width: 2),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Cerrar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                  ],
                ),
              )
              // usually buttons at the bottom of the dialog
            ]);
      },
    );
  }

  Widget _bottomBar(bool tuto) {
    if (tuto) {
      return Container(
        height: 170,
        padding: EdgeInsets.only(left: 40, right: 40, top: 20),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: 40, right: 40, top: 0, bottom: 10),
                    child: Text(
                      'Para agregar más trabajadores haz click en el botón +, cuando termines pulsa el botón "Siguiente paso"',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                    child: SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.yellow[700],
                        ),
                        // padding: EdgeInsets.only(
                        //     left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                        // color: Colors.yellow[700],
                        // elevation: 0.0,
                        // shape: RoundedRectangleBorder(
                        //   borderRadius: new BorderRadius.circular(30.0),
                        // ),
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TutoAsigna()),
                          );
                        },
                        child: Text(
                          'Siguiente paso',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Trabajadores',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[
        // searchBar.getSearchAction(context)
        // IconButton(
        //   icon: const Icon(Icons.filter_alt),
        //   tooltip: 'Show Snackbar',
        //   onPressed: () {
        //     _modalAlias();
        //     // ScaffoldMessenger.of(context).showSnackBar(
        //     //     const SnackBar(content: Text('This is a snackbar')));
        //   },
        // ),
      ],
    );
  }

  _listaTrabajadores() {
    if (data == null) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
        ),
      );
    } else if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              title: Text(item['name'] + " " + item['lastname'],
                  style: TextStyle(
                    color: item['blocked'] ? Colors.red : Colors.black,
                    fontSize: 16.0,
                    // fontWeight: FontWeight.bold,
                  )),
              subtitle: Text(
                item['role']['name'] + (item['blocked'] ? " - bloqueado" : ""),
              ),
              trailing: ClipOval(
                child: Material(
                  color: Colors.grey[300], // button color
                  child: InkWell(
                    splashColor: Colors.yellow[800], // inkwell color
                    child: SizedBox(
                        width: 56, height: 56, child: Icon(Icons.info)),
                    onTap: () {
                      getDataId(ruta, item['id'].toString());
                    },
                  ),
                ),
              ));
        },
      );
    } else {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          // appBar: searchBar.build(context),
          drawer: DrawerAdmin(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevoTrabajador()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: _listaTrabajadores(),
                ),
              ),
              _bottomBar(_tuto),
            ],
          ),
        ));
  }
}
