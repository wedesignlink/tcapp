// ignore_for_file: unnecessary_null_comparison

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:tc_app/camionetas.dart';
import 'package:tc_app/inicio_dashboard.dart';
// import 'package:tc_app/location.dart';
import 'package:tc_app/proyectos.dart';
import 'package:tc_app/reportes.dart';
import 'package:tc_app/shedule.dart';
import 'package:tc_app/trabajadores.dart';
import 'helper.dart';
import 'login.dart';
import 'herramienta.dart';
import 'recibos.dart';

class DrawerAdmin extends StatefulWidget {
  @override
  _MyDrawer createState() => new _MyDrawer();
}

class _MyDrawer extends State<DrawerAdmin> {
  String _name = "";
  String _email = "";
  String _rol = "";
  String _company = "";
  bool _tuto = false;
  String _paso = "";
  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs..clear();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Login()),
    );
  }

  _getVars() async {
    var nombre = await getPreferences("username");
    var correo = await getPreferences("email");
    var rol = await getPreferences("rolename");
    var company = await getPreferences("companyName");
    var tuto = await Helper.getBoolPreferences('tutorial');
    var paso = await Helper.getPreferences('paso');

    setState(() {
      _name = nombre!;
      _email = correo!;
      _rol = rol!;
      _company = company!;
      _tuto = tuto!;
      _paso = paso!;
    });
  }

  @override
  void initState() {
    super.initState();
    _getVars();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    print(_tuto);

    if (_tuto) {
      return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            SizedBox(
              // padding: EdgeInsets.only(left: 20.0),
              // color: Colors.white,
              height: 230,
              child: DrawerHeader(
                  decoration: BoxDecoration(
                    border: Border.all(width: 0, color: Colors.white),
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 10),
                      Image.asset('assets/user.png'),
                      Text(
                        _name != null ? _name : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 13.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _company != null ? _company : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _email != null ? _email : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _rol != null ? _rol : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                    ],
                  )),
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0),
              color: Colors.white,
              height: screenHeight,
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_home.png'),
                    ),
                    title: Text(
                      'Configura tu app',
                      style: TextStyle(
                        color: Color.fromARGB(255, 220, 166, 5),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Helper.linkTuto(_paso, context);
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //reportes
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_reporte.png'),
                    ),
                    title: Text(
                      'Reportes',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => InicioAdmin()),
                      // );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // proyectos
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_proyectos.png'),
                    ),
                    title: Text(
                      'Proyectos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => Proyectos()),
                      // );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // trabajadores
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_trabajadores.png'),
                    ),
                    title: Text(
                      'Trabajadores',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => Trabajadores()),
                      // );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // camionetas
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_camionetas.png'),
                    ),
                    title: Text(
                      'Vehículos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Camionetas()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // shedule
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_shedule.png'),
                    ),
                    title: Text(
                      'Asignaciones',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Shedule()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //herramientas
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_herramientas.png'),
                    ),
                    title: Text(
                      'Control herramienta',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => Herramienta()),
                      // );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //recibos
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_recibos.png'),
                    ),
                    title: Text(
                      'Recibos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 225, 227, 228),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => Recibos()),
                      // );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // location
                  // ListTile(
                  //   leading: Container(
                  //     child: Image.asset('assets/icon_location.png'),
                  //   ),
                  //   title: Text(
                  //     'Live Location',
                  //     style: TextStyle(
                  //       color: Color.fromARGB(255, 225, 227, 228),
                  //       fontSize: 16.0,
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //   ),
                  //   onTap: () {
                  //     // Update the state of the app.
                  //     // ...
                  //     // Navigator.push(
                  //     //   context,
                  //     //   MaterialPageRoute(builder: (context) => Location()),
                  //     // );
                  //   },
                  // ),
                  // const Divider(
                  //   color: Color.fromARGB(255, 244, 244, 244),
                  //   height: 20,
                  //   thickness: 1,
                  //   // indent: 20,
                  //   endIndent: 0,
                  // ),
                  // salir
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_exit.png'),
                    ),
                    title: Text(
                      'Cerrar sesión',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      logoutUser();
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            SizedBox(
              // padding: EdgeInsets.only(left: 20.0),
              // color: Colors.white,
              height: 230,
              child: DrawerHeader(
                  decoration: BoxDecoration(
                    border: Border.all(width: 0, color: Colors.white),
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 10),
                      Image.asset('assets/user.png'),
                      Text(
                        _name != null ? _name : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 13.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _company != null ? _company : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _email != null ? _email : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        _rol != null ? _rol : "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 200, 200, 200),
                          fontSize: 10.0,
                        ),
                      ),
                    ],
                  )),
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0),
              color: Colors.white,
              height: screenHeight,
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_home.png'),
                    ),
                    title: Text(
                      'Escritorio',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => InicioDashboard()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //reportes
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_reporte.png'),
                    ),
                    title: Text(
                      'Reportes',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Reportes()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // proyectos
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_proyectos.png'),
                    ),
                    title: Text(
                      'Proyectos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Proyectos()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // trabajadores
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_trabajadores.png'),
                    ),
                    title: Text(
                      'Trabajadores',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Trabajadores()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // camionetas
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_camionetas.png'),
                    ),
                    title: Text(
                      'Vehículos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Camionetas()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // shedule
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_shedule.png'),
                    ),
                    title: Text(
                      'Asignaciones',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Shedule()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //herramientas
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_herramientas.png'),
                    ),
                    title: Text(
                      'Control herramienta',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Herramienta()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  //recibos
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_recibos.png'),
                    ),
                    title: Text(
                      'Recibos',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app.
                      // ...
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Recibos()),
                      );
                    },
                  ),
                  const Divider(
                    color: Color.fromARGB(255, 244, 244, 244),
                    height: 20,
                    thickness: 1,
                    // indent: 20,
                    endIndent: 0,
                  ),
                  // location
                  // ListTile(
                  //   leading: Container(
                  //     child: Image.asset('assets/icon_location.png'),
                  //   ),
                  //   title: Text(
                  //     'Live Location',
                  //     style: TextStyle(
                  //       color: Color.fromARGB(255, 53, 66, 74),
                  //       fontSize: 16.0,
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //   ),
                  //   onTap: () {
                  //     // Update the state of the app.
                  //     // ...
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(builder: (context) => Location()),
                  //     );
                  //   },
                  // ),
                  // const Divider(
                  //   color: Color.fromARGB(255, 244, 244, 244),
                  //   height: 20,
                  //   thickness: 1,
                  //   // indent: 20,
                  //   endIndent: 0,
                  // ),
                  // salir
                  ListTile(
                    leading: Container(
                      child: Image.asset('assets/icon_exit.png'),
                    ),
                    title: Text(
                      'Cerrar sesión',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      logoutUser();
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }
}
