// ignore_for_file: deprecated_member_use

import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as search;
import 'package:tc_app/finalizar_trabajo.dart';
import 'helper.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevoTrabajo extends StatefulWidget {
  final Map shedule;
  const NuevoTrabajo({Key? key, required this.shedule}) : super(key: key);
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevoTrabajo> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}proyecto-detalles";
  String rutaUpload = "${server}upload";
  TextEditingController proyectoController = TextEditingController();
  TextEditingController comentarioController = TextEditingController();
  DateTime finicio = DateTime.now();
  // late search.SearchBar searchBar;
  List data = [];
  var _image;
  var imagePicker;
  var type;

  void nuevoTrabajo(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "Comentarios": comentarioController.text,
      "user": user!,
      "proyecto": widget.shedule['proyecto']['id'].toString()
    };

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    // print(response.statusCode);
    // print(responseData["id"]);

    if (response.statusCode == 200) {
      comentarioController.clear();

      if (_image != null) {
        Helper.cargarImagen(_image, rutaUpload, responseData["id"],
            'proyecto-detalle', 'Imagenes_inicio', token);
      }

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FinalizarTrabajo(
                shedule: widget.shedule, trabajo: responseData)),
      );
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  uploadCamera() async {
    XFile image = await imagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      _image = File(image.path);
    });

    print(File(image.path));
  }

  void _validateInputs() {
    nuevoTrabajo(ruta);
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    // print(widget.shedule);
  }

  Widget _bloqueImagen() {
    if (_image == null) {
      return Container();
    } else {
      return Center(
          child: Image.file(
        _image,
        fit: BoxFit.fitWidth,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    var proyecto = widget.shedule['proyecto']['titulo'];
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Iniciar Trabajo',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                )),
            actions: <Widget>[],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          child: ListView(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                              child: Text('Proyecto',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                  ))),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            initialValue: proyecto,
                            enabled: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Escribe un concepto'),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                              child: Text('Comentarios',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                  ))),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: comentarioController,
                            maxLines: 3,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Escribe un comentario'),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Center(
                              child: Text('Foto',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                  ))),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                              child: ElevatedButton(
                            onPressed: () {
                              uploadCamera();
                            },
                            child: Icon(Icons.camera, color: Colors.white),
                            style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(30),
                              backgroundColor:
                                  Colors.yellow[700], // <-- Button color
                            ),
                          )),
                          SizedBox(
                            height: 20,
                          ),
                          _bloqueImagen(),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 20),
                            child: ElevatedButton(
                                child: Text('Iniciar trabajo',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14.0,
                                    )),
                                onPressed: () {
                                  _validateInputs();
                                },
                                style: ButtonStyle(
                                    padding:
                                        MaterialStateProperty.all<EdgeInsets>(
                                            EdgeInsets.all(20)),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.black),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    )))),
                          )
                        ],
                      )))),
            ],
          ),
        ));
  }
}
