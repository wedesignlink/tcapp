import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:full_screen_image/full_screen_image.dart';
import 'package:csv/csv.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';
// import 'package:share_plus/share_plus.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tc_app/drawer_trabajador.dart';
import 'helper.dart';
import 'funciones.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class MisTrabajos extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<MisTrabajos> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // String ruta = "${server}shedules";
  String ruta = "${server}proyecto-detalles";
  String trabajadores = "${server}users";
  String proyectos = "${server}proyectos";
  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataProyectos = [];
  List dataUsers = [];
  late List data;
  int _totalPies = 0;
  int _totalHoras = 0;
  // double _precioPie = 0.0;
  double _precioHora = 0.0;
  double _montoTotal = 0.0;
  late String? _dropdownValueUser;
  late String? _dropdownValueProy;
  late Position currentLocation;
  late String _nivel;
  var rol;

  getDataShedule(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');
    var price = await Helper.getPreferences('precio');
    var nivel = await Helper.getPreferences('nivel');

    var filterProy =
        _dropdownValueProy != null ? "&proyecto.id=" + _dropdownValueProy! : "";
    var fechaActual = DateFormat('yyyy-MM-dd').format(DateTime.now());
    var fechaSeleccionadaInicio = DateFormat('yyyy-MM-dd').format(finicio);
    var filterfInicio = fechaActual != fechaSeleccionadaInicio
        ? "&created_at_gt=" + fechaSeleccionadaInicio.toString()
        : "";
    var fechaSeleccionadaFin =
        DateFormat('yyyy-MM-dd').format(ffin.add(Duration(days: 1, hours: 23)));
    var fechaSeleccionadaFinCompara = DateFormat('yyyy-MM-dd').format(ffin);
    // fechaSeleccionadaFin.add(Duration(days: 1, hours: 23));
    var filterfFin = fechaActual != fechaSeleccionadaFinCompara
        ? "&created_at_lt=" + fechaSeleccionadaFin.toString()
        : "";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var rutaFiltro = ruta +
        "?user.id=" +
        user.toString() +
        filterProy +
        filterfInicio +
        filterfFin +
        // "&terminado=true" +
        "&_sort=id:DESC";

    // print(rutaFiltro);

    var response = await http.get(Uri.parse(rutaFiltro), headers: headers);

    final resBody = json.decode(response.body);
    var totalPies = 0;
    var totalHoras = 0;
    double montoTotalPies = 0.0;
    double montoTotalHoras = 0.0;
    double precioPie = 0.0;

    resBody.forEach((item) {
      print("nivel=" + nivel!);
      //getting the key direectly from the name of the key
      if (nivel == "2" || nivel == "3") {
        precioPie = double.parse(item['proyecto']['precio_pie'].toString());
      } else if (nivel == "1") {
        precioPie =
            double.parse(item['proyecto']['precio_pie_supervisor'].toString());
      }

      totalHoras += int.parse(item['Horas']);
      totalPies += int.parse(item["Pies"]);
      montoTotalPies += (item['Pies'] ?? 0) * precioPie;
      montoTotalHoras += (item['Horas'] ?? 0) * double.parse(price!);
    });
    // print(totalHoras);
    // print(double.parse(price));

    // print(montoTotalPies + montoTotalHoras);
    setState(() {
      data = resBody;
      _totalPies = totalPies;
      _totalHoras = totalHoras;
      // _precioPie = precioPie;
      _precioHora = double.parse(price!);
      _montoTotal = montoTotalPies + montoTotalHoras;
      _nivel = nivel!;
    });
  }

  void getDataProyectos(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?user.company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataProyectos = resBody;
    });
  }

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    var price = await Helper.getPreferences('precio');
    var nivel = await Helper.getPreferences('nivel');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var filterUser =
        _dropdownValueUser != null ? "&user.id=" + _dropdownValueUser! : "";
    var filterProy =
        _dropdownValueProy != null ? "&proyecto.id=" + _dropdownValueProy! : "";
    var fechaActual = DateFormat('yyyy-MM-dd').format(DateTime.now());
    var fechaSeleccionadaInicio = DateFormat('yyyy-MM-dd').format(finicio);
    var filterfInicio = fechaActual != fechaSeleccionadaInicio
        ? "&created_at_gt=" + fechaSeleccionadaInicio.toString()
        : "";
    var fechaSeleccionadaFin = DateFormat('yyyy-MM-dd').format(ffin);
    var filterfFin = fechaActual != fechaSeleccionadaFin
        ? "&created_at_lt=" + fechaSeleccionadaFin.toString()
        : "";

    var response = await http.get(
        Uri.parse(ruta +
            "?user.company=" +
            company +
            filterUser +
            filterProy +
            filterfInicio +
            filterfFin +
            "&_sort=created_at:DESC"),
        headers: headers);

    var resBody = json.decode(response.body);

    int totalPies = 0;
    int totalHoras = 0;
    double montoTotalPies = 0.0;
    double montoTotalHoras = 0.0;
    // double precioPie = 0.0;

    resBody.forEach((item) {
      var precioPie;
      //getting the key direectly from the name of the key
      if (nivel == "2") {
        precioPie = double.parse(item['proyecto']['precio_pie'].toString());
      } else if (nivel == "3") {
        precioPie =
            double.parse(item['proyecto']['precio_pie_supervisor'].toString());
      }

      totalPies += int.parse(item["Pies"]);
      totalHoras += int.parse(item["Horas"]);
      montoTotalPies += (item["Pies"] ?? 0) * precioPie;
      montoTotalHoras += (item["Horas"] ?? 0) * double.parse(price!);
    });

    setState(() {
      data = resBody;
      _totalPies = totalPies;
      _totalHoras = totalHoras;
      // _precioPie = precioPie;
      _precioHora = double.parse(price!);
      _montoTotal = montoTotalPies + montoTotalHoras;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(resBody[0]);
  }

  getCsv() async {
    //create an element rows of type list of list. All the above data set are stored in associate list
//Let associate be a model class with attributes name,gender and age and data be a list of associate model class.
    List<dynamic> rowh = [];

    List<List<dynamic>> rows = [];

    rowh.add("Nombre");
    rowh.add("Pies");
    rowh.add("Horas");
    rowh.add("Proyecto");
    rowh.add("Precio proyecto");
    rowh.add("Fecha");
    rowh.add("Comentarios");
    rows.add(rowh);

    for (int i = 0; i < data.length; i++) {
      List<dynamic> row = [];
      //row refer to each column of a row in csv file and rows refer to each row in a file
      row.add(data[i]['user']['name'] + " " + data[i]['user']['lastname']);
      row.add(data[i]['Pies'] ?? "");
      row.add(data[i]['Horas'] ?? "");
      row.add(data[i]['proyecto']['titulo']);
      row.add(data[i]['proyecto']['precio_pie']);
      row.add(data[i]['created_at']);
      row.add(data[i]['Comentarios'] ?? "");

      rows.add(row);
    }

    //store file in documents folder
    final dir = await getApplicationDocumentsDirectory();
    // final dir = await getExternalStorageDirectory();

    // print(dir.path);
    var file = dir.path + "/reporte.csv";
    File f = new File(file);

    // // convert rows to String and write as csv file
    String csv = const ListToCsvConverter().convert(rows);

    f.writeAsString(csv);

    // ignore: deprecated_member_use
    // Share.shareFiles([f.path]);
  }

  Future<void> _selectDate(BuildContext context) async {
    // var formatter = new DateFormat('yyyy-MM-dd');
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != finicio)
      setState(() {
        finicio = pickedDate;
      });
    getDataShedule(ruta);
    Navigator.of(context).pop();
    _modalFilter(finicio, ffin);
  }

  Future<void> _selectDateFin(BuildContext context) async {
    // var formatter = new DateFormat('yyyy-MM-dd');
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != ffin)
      setState(() {
        ffin = pickedDate;
      });
    getDataShedule(ruta);
    Navigator.of(context).pop();
    _modalFilter(finicio, ffin);
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    setState(() {
      rol = role;
    });
  }

  @override
  initState() {
    super.initState();
    getDataShedule(ruta);
    getDataProyectos(proyectos);
  }

  _limpiarFiltros(context) {
    setState(() {
      _dropdownValueProy = null;
      finicio = DateTime.now();
      ffin = DateTime.now();
    });
    getDataShedule(ruta);
    Navigator.pop(context);
  }

  Widget _imagenHerramienta(imagen) {
    if (imagen?.isEmpty ?? true) {
      return Image.asset(
        'assets/not-image.png',
        fit: BoxFit.fitWidth,
      );
    } else {
      return Image.network(
        baseUrl + imagen['url'],
        fit: BoxFit.fitWidth,
      );
    }
  }

  _modalDetalle(data) async {
    final horizontalPadding = 65;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            actionsPadding: EdgeInsets.only(
                right: MediaQuery.of(context).size.width / 2 -
                    horizontalPadding * 2),
            title: Column(children: [
              Text('Detalle recibo'),
              FullScreenWidget(
                disposeLevel: DisposeLevel.High,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(16),
                  child: _imagenHerramienta(data['Imagen']),
                ),
              )
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text("Fecha del recibo: " +
                        data['Fecha'].toString().substring(0, 10)),
                  ),
                  Center(
                    child: Text("Concepto: " + data['Concepto']),
                  ),
                  Center(
                    child: Text(
                        data['user']['name'] + " " + data['user']['lastname']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cancelar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ],
              ),
            ]);
      },
    );
  }

  Widget _fechaMostrar(fecha) {
    return Text(
        fecha.year.toString() +
            "/" +
            fecha.month.toString() +
            "/" +
            fecha.day.toString(),
        style: TextStyle(
          color: Color.fromARGB(255, 53, 66, 74),
          fontSize: 16.0,
          // fontWeight: FontWeight.bold,
        ));
  }

  _modalFilter(finicio, ffin) {
    // final horizontalPadding = 65;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
              title: Column(children: [
                Text('Filtrar'),
              ]),
              content: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        'Proyecto',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Container(
                        child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      validator: (value) => validateDrop(value!),
                      value: _dropdownValueProy,
                      hint: Text('Seleccione un proyecto'),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      onChanged: (newValue) async {
                        setState(() {
                          _dropdownValueProy = newValue;
                        });
                        // getData(ruta);
                        getDataShedule(ruta);
                      },
                      items: dataProyectos.map((item) {
                        return new DropdownMenuItem(
                          child: Text(item['titulo'] ?? ""),
                          value: item['id'].toString(),
                        );
                      }).toList(),
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.black),
                                    shape: WidgetStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ))),
                                onPressed: () => {_selectDate(context)},
                                child: Text(
                                  'Fecha inicio',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                              _fechaMostrar(finicio)
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              // SizedBox(
                              //   height: 20.0,
                              // ),
                              ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.black),
                                    shape: WidgetStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ))),
                                onPressed: () => {_selectDateFin(context)},
                                child: Text(
                                  'Fecha fin',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                              _fechaMostrar(ffin),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                Row(
                  children: [
                    SizedBox(width: 10),
                    Expanded(
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              Colors.orange[400]!),
                        ),
                        onPressed: () {
                          _limpiarFiltros(context);
                        },
                        child: const Text('Limpiar'),
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                        child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        // _guardaAlias();
                      },
                      child: Text(
                        'Cerrar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    )),
                    SizedBox(width: 10),
                  ],
                ),
              ]);
        });
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Mis trabajos',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.filter_alt),
          tooltip: 'Filtrar',
          onPressed: () {
            _modalFilter(finicio, ffin);
            // ScaffoldMessenger.of(context).showSnackBar(
            //     const SnackBar(content: Text('This is a snackbar')));
          },
        ),
      ],
    );
  }

  _mostrarMenu() {
    if (rol == "34") {
      return DrawerAdmin();
    } else {
      return DrawerTrabajador();
    }
  }

  Widget listTileTrabajador(item) {
    if (item['Pies'] != null && (_nivel == "2" || _nivel == "3")) {
      return ListTile(
          title: Text(
              item['Pies'].toString() +
                  " Pies x (\$" +
                  item['proyecto']['precio_pie'].toString() +
                  ") " +
                  item['proyecto']['titulo'],
              style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
              )),
          subtitle: Text(
            item['created_at'].toString().substring(0, 10),
          ),
          trailing: ClipOval(
              child: Material(
            color: Colors.green[300], // button color
            child: InkWell(
              splashColor: Colors.yellow[800], // inkwell color
              child: SizedBox(width: 56, height: 56, child: Icon(Icons.check)),
              onTap: () {
                // getDataId(ruta, item['id'].toString());
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => NuevoTrabajo(shedule: shedule)),
                // );
              },
            ),
          )));
    }
    if (item['Pies'] != null && _nivel == "1") {
      return ListTile(
          title: Text(
              item['Pies'].toString() +
                  " Pies x (\$" +
                  item['proyecto']['precio_pie_supervisor'].toString() +
                  ") " +
                  item['proyecto']['titulo'],
              style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
              )),
          subtitle: Text(
            item['created_at'].toString().substring(0, 10),
          ),
          trailing: ClipOval(
              child: Material(
            color: Colors.green[300], // button color
            child: InkWell(
              splashColor: Colors.yellow[800], // inkwell color
              child: SizedBox(width: 56, height: 56, child: Icon(Icons.check)),
              onTap: () {
                // getDataId(ruta, item['id'].toString());
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => NuevoTrabajo(shedule: shedule)),
                // );
              },
            ),
          )));
    } else if (item['Horas'] != null) {
      return ListTile(
          title: Text(
              item['Horas'].toString() +
                  " Horas x " +
                  "(\$" +
                  (_precioHora).toStringAsFixed(2) +
                  ")" +
                  item['proyecto']['titulo'],
              style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
              )),
          subtitle: Text(
            item['created_at'].toString().substring(0, 10),
          ),
          trailing: ClipOval(
              child: Material(
            color: Colors.green[300], // button color
            child: InkWell(
              splashColor: Colors.yellow[800], // inkwell color
              child: SizedBox(width: 56, height: 56, child: Icon(Icons.check)),
              onTap: () {
                // getDataId(ruta, item['id'].toString());
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => NuevoTrabajo(shedule: shedule)),
                // );
              },
            ),
          )));
    } else {
      return Container();
    }
  }

  Widget listaCreate() {
    if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return listTileTrabajador(item);
        },
      );
    } else {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    }
  }

  Widget _resumen() {
    print(_montoTotal);
    if (_montoTotal > 0) {
      return Container(
        height: 170,
        padding: EdgeInsets.only(left: 40, right: 40, top: 20),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  Text("Total de Pies:" + _totalPies.toString()),
                  Text("Total de Horas:" +
                      _totalHoras.toString() +
                      "(\$" +
                      (_precioHora).toStringAsFixed(2) +
                      ")"),
                  Divider(color: Colors.black),
                  Text(
                    "\$" + (_montoTotal).toStringAsFixed(2),
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.share),
                        onPressed: () {
                          getCsv();
                        },
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: deprecated_member_use
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: _mostrarMenu(),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: listaCreate(),
              )),
              _resumen()
            ],
          ),
        ));
  }
}
