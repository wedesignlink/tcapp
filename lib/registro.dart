// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as search;
import 'package:tc_app/registro_company.dart';
import 'funciones.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class Registro extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<Registro> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}recibos";
  TextEditingController nombreController = TextEditingController();
  TextEditingController apellidoController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController passConfController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  // late search.SearchBar searchBar;

  void siguientePaso() async {
    Map<String, String> values = {
      "name": nombreController.text,
      "lastname": apellidoController.text,
      "email": emailController.text,
      "password": passController.text,
      // "user": user
    };

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegistroCompany(newUser: values)),
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      siguientePaso();
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    // navigateUser();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Registro',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                )),
            actions: <Widget>[],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              Center(
                                child: Text('Paso 1/2'),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Text(
                                  "Registrate como administrador para configurar tu aplicación"),
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: Text('Nombre',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: nombreController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Nombre'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                  child: Text('Apellido',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: apellidoController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Apellido'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                  child: Text('Email',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: emailController,
                                validator: (value) => validateEmail(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'escriba su email'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                  child: Text('Contraseña',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: passController,
                                validator: (value) => validatePass(value!),
                                obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: '8 dígitos números y letras'),
                              ),
                              // SizedBox(
                              //   height: 20,
                              // ),
                              // Center(
                              //     child: Text('Confirmar Contraseña',
                              //         style: TextStyle(
                              //           color: Colors.black,
                              //           fontSize: 14.0,
                              //         ))),
                              // SizedBox(
                              //   height: 10,
                              // ),
                              // TextFormField(
                              //   controller: passConfController,
                              //   validator: validatePass,
                              //   obscureText: true,
                              //   enableSuggestions: false,
                              //   autocorrect: false,
                              //   decoration: InputDecoration(
                              //       border: OutlineInputBorder(),
                              //       hintText: 'Ingrese de nuevo la contraseña'),
                              // ),
                              SizedBox(
                                height: 30,
                              ),
                              Container(
                                child: ElevatedButton(
                                    child: Text('Continuar'),
                                    onPressed: () {
                                      _validateInputs();
                                    },
                                    style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(15)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.grey),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                        )))),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                            ],
                          )))),
            ],
          ),
        ));
  }
}
