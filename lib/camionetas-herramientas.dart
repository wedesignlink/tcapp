// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/camionetas.dart';

import 'helper.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class CamionetasHerramientas extends StatefulWidget {
  final String id;
  const CamionetasHerramientas({Key? key, required this.id}) : super(key: key);
  @override
  _MyStart createState() => new _MyStart();
}

// Widget get _loadingView {
//   return new Center(
//     child: new CircularProgressIndicator(),
//   );
// }

class _MyStart extends State<CamionetasHerramientas>
    with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController nombreController = TextEditingController();
  TextEditingController cantidadController = TextEditingController();
  String ruta = "${server}herramientas";
  List data = [];
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  bool _autoValidate = false;
  bool _tuto = false;

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void getDataId(ruta, id) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?camioneta.id=" + id),
        headers: headers);

    var resBody = json.decode(response.body);
    // print(resBody);
    var tutorial = await Helper.getBoolPreferences('tutorial') ?? false;
    setState(() {
      data = resBody;
      _tuto = tutorial;
    });
  }

  void _delHerramienta(ruta, id) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.delete(Uri.parse(ruta + "/" + id), headers: headers);

    final responseData = json.decode(response.body);

    print(response.statusCode);
    print(responseData);

    if (response.statusCode == 200) {
      onSubmitted("Se ha eliminado el elemento");
      getDataId(ruta, widget.id);
    } else {
      onSubmitted("Error!, No se ha borrado el elemento");
    }
  }

  void guardarProyecto(ruta, id) async {
    var token = await getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map values = {
      "Nombre": nombreController.text,
      "Cantidad": cantidadController.text,
      "camioneta": int.parse(id),
    };

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => CamionetasHerramientas(id: id)),
      );
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  Future<Future<ConfirmAction?>> _asyncConfirmDialog(
      BuildContext context, ruta, id) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Desea eliminar el elemento?'),
          content: const Text('Una vez eliminado no podrá revertise'),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCELAR'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.CANCEL);
              },
            ),
            TextButton(
              child: const Text('CONFIRMAR'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.ACCEPT);
                _delHerramienta(ruta, id);
              },
            )
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    getDataId(ruta, widget.id);
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarProyecto(ruta, widget.id.toString());
    } else {
      _showDialog('Ups!', 'Debe completar el formulario correctamente');

      setState(() {
        _autoValidate = true;
      });
    }
  }

  _modalNueva() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(children: [
              Text('Nueva herramienta'),
            ]),
            content: SingleChildScrollView(
                child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: nombreController,
                    // validator: validateName,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), hintText: 'Nombre'),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: cantidadController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), hintText: 'Cantidad'),
                  ),
                ],
              ),
            )),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  SizedBox(width: 20),
                  Expanded(
                    child: OutlinedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Cancelar'),
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        _validateInputs();
                      },
                      child: Text(
                        'Guardar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                ],
              ),
            ]);
      },
    );
  }

  Widget _bottomBar(bool tuto) {
    if (tuto) {
      return Container(
        height: 100,
        padding: EdgeInsets.only(left: 40, right: 40, top: 20),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: 40, right: 40, top: 0, bottom: 10),
                    child: Text(
                      'Para agregar herramientas haz click en el botón +, cuando termines pulsa el botón "Regresar" del menu superior <.',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                  //   child: SizedBox(
                  //     width: double.infinity,
                  //     // ignore: deprecated_member_use
                  //     child: RaisedButton(
                  //       padding: EdgeInsets.only(
                  //           left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                  //       color: Colors.yellow[700],
                  //       elevation: 0.0,
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: new BorderRadius.circular(30.0),
                  //       ),
                  //       onPressed: () async {
                  //         Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => TutoBasic()),
                  //         );
                  //       },
                  //       child: Text(
                  //         'Siguiente paso',
                  //         style: TextStyle(
                  //           color: Colors.black,
                  //           fontWeight: FontWeight.bold,
                  //           fontFamily: 'Montserrat',
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: const Icon(Icons.arrow_back_ios),
        tooltip: 'Increase volume by 10',
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Camionetas()),
          );
        },
      ),
      title: Text('Herramientas',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          )),
      actions: <Widget>[
        // searchBar.getSearchAction(context)
      ],
    );
  }

  Widget _datalist() {
    if (data.length == 0) {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    } else {
      return ListView.separated(
        itemCount: data.length,
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              title: Text(item["Cantidad"].toString() + " " + item['Nombre']),
              onTap: () {
                // _modalDetalle(data[0]);
              },
              trailing: ClipOval(
                child: Material(
                  color: Colors.grey[400], // button color
                  child: InkWell(
                    splashColor: Colors.yellow[800], // inkwell color
                    child: SizedBox(
                        width: 32, height: 32, child: Icon(Icons.clear)),
                    onTap: () {
                      // _delHerramienta(ruta, item['id'].toString());
                      _asyncConfirmDialog(context, ruta, item['id'].toString());
                    },
                  ),
                ),
              ));
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) => NuevoTrabajador()),
              // );
              _modalNueva();
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              // Container(
              //   // ignore: deprecated_member_use
              //   child: RaisedButton(
              //     child: Text('Back'),
              //     onPressed: () {
              //       Navigator.pop(context);
              //     },
              //   ),
              // ),
              Expanded(
                  child: Container(
                padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: _datalist(),
              )),
              _bottomBar(_tuto),
            ],
          ),
        ));
  }
}
