// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'helper.dart';
import 'package:full_screen_image/full_screen_image.dart';

import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class DetalleProyecto extends StatefulWidget {
  final String id;
  const DetalleProyecto({Key? key, required this.id}) : super(key: key);
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<DetalleProyecto> with WidgetsBindingObserver {
  String ruta = "${server}proyecto-detalles";
  String baseUrl = "http://159.203.164.232";
  late List data;
  late String nivel;
  late String precio;

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');
    var nivelu = await Helper.getPreferences('nivel');
    var preciou = await Helper.getPreferences('precio') != "null"
        ? await Helper.getPreferences('precio')
        : 0;

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      data = resBody;
      nivel = nivelu!;
      precio = preciou.toString();
    });
  }

  @override
  void initState() {
    getDataId(ruta, widget.id);
    super.initState();
  }

  // Future<Null> shareScreenshot() async {
  //   try {
  //     RenderRepaintBoundary boundary =
  //         _globalKey.currentContext.findRenderObject();
  //     if (boundary.debugNeedsPaint) {
  //       Timer(Duration(seconds: 2), () => shareScreenshot());
  //       return null;
  //     }
  //     ui.Image image = await boundary.toImage();
  //     final directory = (await getExternalStorageDirectory()).path;
  //     ByteData byteData =
  //         await image.toByteData(format: ui.ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();
  //     File imgFile = new File('$directory/screenshot.png');
  //     imgFile.writeAsBytes(pngBytes);
  //     final RenderBox box = context.findRenderObject();
  //     Share.shareFiles(['$directory/screenshot.png'],
  //         subject: 'Share ScreenShot',
  //         text: 'Hello, check your share files!',
  //         sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  //   } on PlatformException catch (e) {
  //     print("Exception while taking screenshot:" + e.toString());
  //   }
  // }

  // AppBar buildAppBar(BuildContext context) {
  //   return new AppBar(
  //     centerTitle: true,
  //     title: Text('Detalle proyecto',
  //         style: TextStyle(
  //           color: Colors.white,
  //           fontSize: 16.0,
  //           // fontWeight: FontWeight.bold,
  //         )),
  //     actions: <Widget>[],
  //   );
  // }

  Widget _precio() {
    var monto;
    if (data[0]['Pies'] != null) {
      monto = (data[0]['proyecto']['precio_pie'] != null
              ? data[0]['proyecto']['precio_pie']
              : 0) *
          data[0]['Pies'];
      return Text('\$' + monto.toString(),
          style: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
          ));
    } else if (data[0]['Horas'] != null) {
      monto = (precio != "null" ? double.parse(precio) : 0) * data[0]['Horas'];
      return Text('\$' + monto.toString(),
          style: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
          ));
    } else {
      return Text('');
    }
  }

  // ignore: unused_element
  Widget _imagen(imagen) {
    if (imagen?.isEmpty ?? true) {
      return Image.asset(
        'assets/not-image.png',
        fit: BoxFit.fitWidth,
      );
    } else {
      return Image.network(
        baseUrl + imagen[0]['url'],
        fit: BoxFit.fitWidth,
      );
    }
  }

  List<Widget> _listaImagenes() {
    List<Widget> listings = [];

    if (data[0]['Imagenes_inicio'].length > 0) {
      int i = 0;
      for (i = 0; i < data[0]['Imagenes_inicio'].length; i++) {
        listings.add(FullScreenWidget(
          disposeLevel: DisposeLevel.High,
          child: ClipRRect(
            // borderRadius: BorderRadius.circular(16),
            child: Image.network(
              baseUrl + data[0]['Imagenes_inicio'][i]['url'],
              fit: BoxFit.fitWidth,
            ),
          ),
        ));
      }
      return listings;
    } else {
      return [];
    }
  }

  List<Widget> _listaImagenesF() {
    // <<<<< Note this change for the return type
    // ignore: deprecated_member_use
    List<Widget> listings = [];

    if (data[0]['imagenes_fin'].length > 0) {
      int i = 0;
      for (i = 0; i < data[0]['imagenes_fin'].length; i++) {
        listings.add(FullScreenWidget(
          disposeLevel: DisposeLevel.High,
          child: ClipRRect(
            // borderRadius: BorderRadius.circular(16),
            child: Image.network(
              baseUrl + data[0]['imagenes_fin'][i]['url'],
              fit: BoxFit.fitWidth,
            ),
          ),
        ));
      }
      return listings;
    } else {
      return [];
    }
  }

  Widget _tipo() {
    var tipo;

    if (data[0]["Horas"] != null)
      tipo = "Por Hora";
    else if (data[0]["Pies"] != null) tipo = "Por Pie";

    return Text(data != null ? tipo : "",
        style: TextStyle(
          color: Colors.black,
          fontSize: 14.0,
          // fontWeight: FontWeight.bold,
        ));
  }

  Widget dataCard() {
    if (data[0]["Pies"] != null) {
      return Text('Pies:' + data[0]["Pies"].toString(),
          style: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          ));
    } else if (data[0]["Horas"] != null) {
      return Text('Hrs:' + data[0]["Horas"].toString(),
          style: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          ));
    } else {
      return Text('');
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    // ignore: deprecated_member_use
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          // key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Detalle proyecto',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                )),
            actions: <Widget>[
              // IconButton(
              //   icon: const Icon(Icons.share),
              //   tooltip: 'Compartir',
              //   onPressed: () {
              //     // ScreenshotShareImage.takeScreenshotShareImage();
              //     // shareScreenshot();
              //     // ScreenshotShareImage.takeScreenshotShareImage();
              //     // _modalAlias();
              //     // ScaffoldMessenger.of(context).showSnackBar(
              //     //     const SnackBar(content: Text('This is a snackbar')));
              //   },
              // ),
            ],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: ListView(
                        children: [
                          Row(
                            children: <Widget>[
                              SizedBox(width: screenWidth / 2),
                              Text(
                                  data != null
                                      ? data[0]['created_at']
                                          .toString()
                                          .substring(0, 10)
                                      : "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.0,
                                  ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: screenWidth / 1.5,
                                child: Text(
                                    data != null
                                        ? data[0]['proyecto']['titulo']
                                        : "",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    )),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              _tipo(),
                            ],
                          ),
                          Text(data != null
                              ? data[0]['user']['name'] +
                                  " " +
                                  data[0]['user']['lastname']
                              : ""),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                              data != null
                                  ? data[0]["Comentarios"] != null
                                      ? data[0]["Comentarios"]
                                      : "Sin Comentarios"
                                  : "",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13.0,
                                // fontWeight: FontWeight.bold,
                              )),
                          SizedBox(
                            height: 25,
                          ),
                          Center(
                            child: Text('Trabajo realizado',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  // fontWeight: FontWeight.bold,
                                )),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Card(
                                child: Padding(
                                    padding: EdgeInsets.all(screenWidth / 9),
                                    child: dataCard()),
                              ),
                              Card(
                                child: Padding(
                                    padding: EdgeInsets.all(screenWidth / 9),
                                    child: _precio()),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: Text('Imagen inicial',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  // fontWeight: FontWeight.bold,
                                )),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: _listaImagenes(),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: Text('Imágenes finales',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  // fontWeight: FontWeight.bold,
                                )),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: _listaImagenesF(),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                        ],
                      ))),
            ],
          ),
        ));
  }
}
