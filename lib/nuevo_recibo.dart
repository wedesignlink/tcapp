// ignore_for_file: deprecated_member_use

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as search;
import 'package:tc_app/recibos.dart';
import 'package:tc_app/recibos_trabajador.dart';
import 'helper.dart';
import 'funciones.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevoRecibo extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevoRecibo> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}recibos";
  String rutaUpload = "${server}upload";
  TextEditingController conceptoController = TextEditingController();
  TextEditingController montoController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  DateTime finicio = DateTime.now();
  // late search.SearchBar searchBar;
  var _image;
  var imagePicker;
  var type;
  var rol;

  // ImagePicker picker = ImagePicker();

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    setState(() {
      rol = role;
    });
  }

  void guardarRecibo(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');
    var rol = await Helper.getPreferences('roleid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "Concepto": conceptoController.text,
      "Monto": montoController.text,
      "Fecha": finicio.toString(),
      "user": user!
    };

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    print(response.statusCode);
    print(responseData["id"]);

    if (response.statusCode == 200) {
      // setState(() => _loading = false);
      // _showDialog('OK!', 'Se guardado el usuario correctamente');
      conceptoController.clear();
      montoController.clear();

      print(_image);
      print(rutaUpload);
      print(responseData["id"]);

      Helper.cargarImagen(
          _image, rutaUpload, responseData["id"], 'recibos', 'Imagen', token);

      if (rol == '34') {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Recibos()),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => RecibosTrabajador()),
        );
      }
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  cargarImagen(image, rutaUpload, id, ref, field) async {
    var token = await Helper.getPreferences('jwt');

    var name = "image" + Helper.getRandomString(15);

    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token'
    };
    print(token);
    var uri = Uri.parse(rutaUpload);
    var request = http.MultipartRequest('POST', uri)
      ..fields['ref'] = ref
      ..fields['refId'] = id.toString()
      ..fields['field'] = field
      ..files.add(new http.MultipartFile.fromBytes(
          'files', image.readAsBytesSync(),
          filename: name + ".${image.path.split(".").last}",
          contentType: MediaType("image", "${image.path.split(".").last}")));

    print(image.path.split(".").last);
    print("test.${image.path.split(".").last}");

    request.headers.addAll(headers);
    var response = await request.send();

    print(response.statusCode);
    print(response.reasonPhrase);
    print(response.request);

    if (response.statusCode == 200) {
      print("true");
      return true;
    } else {
      print("false");
      return false;
    }
  }

  uploadCamera() async {
    XFile image = await imagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      _image = File(image.path);
    });

    print(File(image.path));
  }

  uploadDevice() async {
    XFile image = await imagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(image.path);
    });

    print(File(image.path));
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarRecibo(ruta);
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    // navigateUser();
  }

  Widget _bloqueImagen() {
    if (_image == null) {
      return Center(
          child: ElevatedButton(
        onPressed: () {
          // uploadDevice();
          uploadCamera();
        },
        child: Icon(Icons.camera, color: Colors.white),
        style: ElevatedButton.styleFrom(
          shape: CircleBorder(),
          padding: EdgeInsets.all(30),
          backgroundColor:
              Colors.yellow[700], // <-- Button color // <-- Splash color
        ),
      ));
    } else {
      return Center(
          child: Image.file(
        _image,
        fit: BoxFit.fitWidth,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Nuevo Recibo',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[
              // IconButton(
              //   icon: const Icon(Icons.share),
              //   tooltip: 'Compartir',
              //   onPressed: () {
              //     // _modalAlias();
              //     // ScaffoldMessenger.of(context).showSnackBar(
              //     //     const SnackBar(content: Text('This is a snackbar')));
              //   },
              // ),
            ],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: Text('Concepto',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: conceptoController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe un concepto'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                  child: Text('Monto',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: montoController,
                                validator: (value) => validateNumber(value!),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe un monto'),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              Center(
                                  child: Text('Foto',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              _bloqueImagen(),
                              SizedBox(
                                height: 40,
                              ),
                              Container(
                                child: ElevatedButton(
                                    child: Text('Guardar',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Montserrat',
                                        )),
                                    onPressed: () {
                                      _validateInputs();
                                    },
                                    style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(20)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        )))),
                              )
                            ],
                          )))),
            ],
          ),
        ));
  }
}
