import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
// // import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:tc_app/camionetas.dart';
import 'package:tc_app/herramienta.dart';
import 'package:tc_app/proyectos.dart';
import 'package:tc_app/recibos.dart';
import 'package:tc_app/reportes.dart';
import 'package:tc_app/shedule.dart';
import 'package:tc_app/trabajadores.dart';
import 'package:tc_app/tuto_basic.dart';
import 'drawer_trabajador.dart';
import 'helper.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class InicioDashboard extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<InicioDashboard> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}proyecto-detalles";
  String trabajadores = "${server}users";
  String proyectos = "${server}proyectos";
  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataProyectos = [];
  List dataUsers = [];
  List data = [];
  var rol;
  var paso;

  void getDataTrabajadores(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataUsers = resBody;
    });
  }

  void getDataProyectos(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?user.company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataProyectos = resBody;
    });
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  void _actualizarAviso() async {
    var id = await Helper.getPreferences('userid');
    var token = await Helper.getPreferences('jwt');
    var ruta = "${server}users";
    var aviso = "true";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"aviso": aviso};

    print(values);

    var response = await http.put(Uri.parse(ruta + "/" + id!),
        body: json.encode(values), headers: headers);

    print(response.statusCode);

    if (response.statusCode == 200) {
      onSubmitted("Ok, El usuario se ha actualizado");
      await Helper.savePreferencesBool('aviso', true);

      Navigator.pop(context);
    } else {
      onSubmitted("Error!, El Usuario no se ha actualizado");
      Navigator.pop(context);
    }
  }

  void verificar() async {
    var aceptado = await Helper.getBoolPreferences('aviso');
    if (!aceptado!) {
      _aviso();
    }
    // var tuto = await Helper.getBoolPreferences('tutorial');
    // if (!tuto) {
    //   Navigator.push(
    //     context,
    //     MaterialPageRoute(builder: (context) => InicioDashboard()),
    //   );
    // }
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    var pas = await Helper.getPreferences("paso");
    setState(() {
      rol = role;
      paso = pas;
    });
  }

  @override
  initState() {
    super.initState();
    verificar();
    getDataTrabajadores(trabajadores);
    getDataProyectos(proyectos);
    getRol();
  }

  _aviso() async {
    WidgetsBinding.instance.addPostFrameCallback((_) => _modalPrivacidad());
  }

  _modalPrivacidad() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            title: new Text('Aviso de privacidad'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text('Debe aceptar el aviso de privacidad'),
                  // Linkify(
                  //   text: "Consultar el aviso https://cretezy.com",
                  //   options: LinkifyOptions(humanize: false),
                  // )
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              // ignore: deprecated_member_use
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                ),
                // padding: EdgeInsets.only(
                //     left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                // color: Colors.black,
                // shape: RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(25.0),
                //     side: BorderSide(color: Color.fromARGB(255, 254, 153, 2))),
                onPressed: () {
                  // Navigator.pop(context);
                  _actualizarAviso();
                  // _AceptarAviso();
                },
                child: Text(
                  'Acepto el aviso',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ]);
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Escritorio',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      // actions: <Widget>[
      //   // searchBar.getSearchAction(context)
      //   IconButton(
      //     icon: const Icon(Icons.filter_alt),
      //     tooltip: 'Filtrar',
      //     onPressed: () {
      //       _modalFilter(finicio, ffin);
      //       // ScaffoldMessenger.of(context).showSnackBar(
      //       //     const SnackBar(content: Text('This is a snackbar')));
      //     },
      //   ),
      // ],
    );
  }

  _mostrarMenu() {
    if (rol == "34") {
      return DrawerAdmin();
    } else {
      return DrawerTrabajador();
    }
  }

  _activarTutorial() async {
    await Helper.actualizarBoolUser("tutorial", true);
    onSubmitted("Ok, El tutorial se ha iniciado");
    await Helper.savePreferencesBool("tutorial", true);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TutoBasic()),
    );
  }

  Widget dashboard() {
    return Center(
        child: Container(
      padding: EdgeInsets.only(left: 40.0, right: 40.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/logo_large.png"),
          fit: BoxFit.scaleDown,
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // ignore: deprecated_member_use
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: _mostrarMenu(),
          // resizeToAvoidBottomInset: false,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              _activarTutorial();
            },
            child: const Icon(Icons.help_outline),
            backgroundColor: Colors.yellow[700],
          ),
          body: Container(
              // padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
              decoration: BoxDecoration(
                color: Colors.white,
                // image: DecorationImage(
                //   image: AssetImage("assets/fondo_tuto.png"),
                //   fit: BoxFit.contain,
                //   alignment: Alignment.bottomCenter,
                // ),
              ),
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Reportes()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.account_tree_rounded,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Reportes',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Proyectos()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.assignment,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Proyectos',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Trabajadores()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.supervisor_account,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Trabajadores',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Camionetas()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.fire_truck,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Vehículos',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Shedule()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.calendar_month,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Asignaciones',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Herramienta()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.plumbing_outlined,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Herramientas',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Recibos()),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.post_add,
                            color: Colors.black,
                          ),
                          SizedBox(height: 20),
                          Text('Recibos',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                  ),
                  // Container(
                  //   padding: const EdgeInsets.all(8),
                  //   child: ElevatedButton(
                  //     style: ElevatedButton.styleFrom(
                  //       backgroundColor: Colors.orange,
                  //       shape: RoundedRectangleBorder(
                  //           borderRadius: BorderRadius.circular(20.0)),
                  //     ),
                  //     onPressed: () {
                  //       Navigator.push(
                  //         context,
                  //         MaterialPageRoute(builder: (context) => Location()),
                  //       );
                  //     },
                  //     child: Column(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       children: [
                  //         Icon(
                  //           Icons.pin_drop_sharp,
                  //           color: Colors.black,
                  //         ),
                  //         SizedBox(height: 20),
                  //         Text('Ubicación en vivo',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontWeight: FontWeight.bold,
                  //               fontFamily: 'Montserrat',
                  //             ),
                  //             textAlign: TextAlign.center)
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              ))),
    );
  }
}
