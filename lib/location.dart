// ignore_for_file: unnecessary_null_comparison

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tc_app/tools/constants.dart';
import 'helper.dart';
import 'funciones.dart';

class Location extends StatefulWidget {
  @override
  _MyLocationState createState() => new _MyLocationState();
}

class _MyLocationState extends State<Location> {
  // ignore: unused_field
  Completer<GoogleMapController> _controller = Completer();
  String trabajadores = "${server}users";
  final Map<String, Marker> _markers = {};
  late String? _dropdownValueUser;
  List dataUsers = [];

  void getDataTrabajadores(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?company=" + company),
        headers: headers);

    if (response.statusCode == 200) {
      var resBody = json.decode(response.body);

      setState(() {
        dataUsers = resBody;
      });
    }
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    // getDataTrabajadores(trabajadores);
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    var filterUser =
        _dropdownValueUser != null ? "&user.id=" + _dropdownValueUser! : "";

    var response = await http.get(
        Uri.parse("${server}users?company=" + company! + filterUser),
        headers: headers);

    if (response.statusCode == 200) {
      final resBody = json.decode(response.body);

      setState(() {
        _markers.clear();
        for (final user in resBody) {
          if (user['lat'] != null && user['long'] != null) {
            final marker = Marker(
              markerId: MarkerId(user['id'].toString()),
              position:
                  LatLng(double.parse(user['lat']), double.parse(user['long'])),
              infoWindow: InfoWindow(
                title: user['name'] + " " + user['lastname'],
                // snippet: user.address,
              ),
            );

            _markers[user['name']] = marker;
          }
        }
      });
    }
  }

  // ignore: unused_field
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  // ignore: unused_field
  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  _limpiarFiltros(context) {
    setState(() {
      _dropdownValueUser = null;
    });
    // getData(ruta);
    Navigator.pop(context);
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Live location',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[
        // IconButton(
        //   icon: const Icon(Icons.filter_alt),
        //   tooltip: 'Filtrar',
        //   onPressed: () {
        //     _modalFilter();
        //   },
        // )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    getDataTrabajadores(trabajadores);
  }

  // ignore: unused_element
  _modalFilter() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
              title: Column(children: [
                Text('Filtrar'),
              ]),
              content: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        'Trabajador',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Container(
                        child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      validator: (value) {
                        validateDrop(value!);
                        return null;
                      },
                      value: _dropdownValueUser,
                      hint: Text('Seleccione un trabajador'),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      onChanged: (newValue) async {
                        setState(() {
                          _dropdownValueUser = newValue;
                        });
                        // getData(ruta);
                      },
                      items: dataUsers.map((item) {
                        return new DropdownMenuItem(
                          child: Text(item['name'] + " " + item['lastname']),
                          value: item['id'].toString(),
                        );
                      }).toList(),
                    )),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                Row(
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.orange[400]!),
                      ),
                      onPressed: () {
                        _limpiarFiltros(context);
                      },
                      child: const Text('Limpiar'),
                    ),
                    SizedBox(width: 20),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        // _guardaAlias();
                      },
                      child: Text(
                        'Cerrar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ],
                ),
              ]);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: buildAppBar(context),
      body: GoogleMap(
          mapType: MapType.normal,
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          initialCameraPosition: CameraPosition(
              target: LatLng(20.6739383, -103.4054535), zoom: 13),
          cameraTargetBounds: _markers.values.toSet() == null
              ? CameraTargetBounds.unbounded
              : CameraTargetBounds(_bounds(_markers.values.toSet())),
          markers: _markers.values.toSet(),
          onMapCreated: _onMapCreated),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Location()),
          );
        },
        label: Text('Update'),
        icon: Icon(Icons.update),
      ),
    );
  }

  LatLngBounds? _bounds(Set<Marker> markers) {
    if (markers == null || markers.isEmpty) return null;
    return _createBounds(markers.map((m) => m.position).toList());
  }
}

LatLngBounds _createBounds(List<LatLng> positions) {
  final southwestLat = positions.map((p) => p.latitude).reduce(
      (value, element) => value < element ? value : element); // smallest
  final southwestLon = positions
      .map((p) => p.longitude)
      .reduce((value, element) => value < element ? value : element);
  final northeastLat = positions
      .map((p) => p.latitude)
      .reduce((value, element) => value > element ? value : element); // biggest
  final northeastLon = positions
      .map((p) => p.longitude)
      .reduce((value, element) => value > element ? value : element);
  return LatLngBounds(
      southwest: LatLng(southwestLat, southwestLon),
      northeast: LatLng(northeastLat, northeastLon));
}
