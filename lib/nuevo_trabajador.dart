// import 'package:donchonito_app/recibido.dart';

// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/helper.dart';
import 'funciones.dart';
import 'login.dart';
import 'tools/constants.dart';
import 'trabajadores.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevoTrabajador extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevoTrabajador> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController nombreController = TextEditingController();
  TextEditingController apellidoController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController precioController = TextEditingController();
  TextEditingController passController = TextEditingController();
  String niveles = "${server}nivels";
  String roles = "${server}users-permissions/roles";
  String ruta = "${server}users";
  List dataNiveles = [];
  List dataRoles = [];
  late String _dropdownValueNivel;
  late String _dropdownValueRole;
  final _formKey = GlobalKey<FormState>();
  var _autoValidate = AutovalidateMode.disabled;

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void getDataRole(ruta) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta), headers: headers);

    var resBody = json.decode(response.body);

    var roles = resBody['roles'];

    roles.removeWhere((item) => item['id'] == 1);

    print(roles);

    setState(() {
      dataRoles = roles;
    });
    // print(dataRoles);
  }

  void getDataNiveles(ruta) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta), headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataNiveles = resBody;
    });
    // print(dataNiveles);
  }

  void guardarTrabajador(ruta) async {
    var token = await getPreferences('jwt');
    var company = await getPreferences('company');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "name": nombreController.text,
      "lastname": apellidoController.text,
      "email": emailController.text,
      "username": nombreController.text + "_" + apellidoController.text,
      "price": precioController.text,
      "role": _dropdownValueRole,
      "nivel": _dropdownValueNivel,
      "password": passController.text,
      "confirmed": "true",
      "blocked": "false",
      "company": company!
    };

    print(values);

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    print(response.statusCode);
    print(responseData);

    if (response.statusCode == 201) {
      // setState(() => _loading = false);
      _showDialog('OK!', 'Se guardado el usuario correctamente');
      cleanForm();
      Helper.actualizarPaso("2");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Trabajadores()),
      );
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  void cleanForm() {
    nombreController.clear();
    apellidoController.clear();
    precioController.clear();
    emailController.clear();
    _dropdownValueRole = "";
    _dropdownValueNivel = "";
    passController.clear();
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarTrabajador(ruta);
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    navigateUser();
    getDataRole(roles);
    getDataNiveles(niveles);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Nuevo Trabajador',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: nombreController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Nombre'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                validator: (value) => validateName(value!),
                                controller: apellidoController,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Apellido'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                validator: (value) => validateEmail(value!),
                                controller: emailController,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Email'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                validator: (value) => validatePass(value!),
                                obscureText: true,
                                controller: passController,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Password'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Rol',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                isExpanded: true,
                                validator: (value) => validateDrop(value!),
                                value: _dropdownValueRole,
                                hint: Text('Seleccione un rol'),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _dropdownValueRole = newValue!;
                                  });
                                },
                                items: dataRoles.map((item) {
                                  // if (item['name'] != "Administrador") {
                                  return new DropdownMenuItem(
                                    child: new Text(item['name']),
                                    value: item['id'].toString(),
                                  );
                                  // }
                                }).toList(),
                              )),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Nivel',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              // Text(dataNiveles[0]['name']),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                validator: (value) => validateDrop(value!),
                                isExpanded: true,
                                value: _dropdownValueNivel,
                                hint: Text('Seleccione un nivel'),
                                // icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) {
                                  setState(() {
                                    _dropdownValueNivel = newValue!;
                                  });
                                },
                                items: dataNiveles.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(item['name']),
                                    value: item['id'].toString(),
                                  );
                                }).toList(),
                              )),
                              SizedBox(
                                height: 30,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                validator: (value) => validateNumber(value!),
                                controller: precioController,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Precio por hora'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  child: Text('Guardar usuario',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                      )),
                                  onPressed: () {
                                    _validateInputs();
                                  },
                                  style: ButtonStyle(
                                      padding:
                                          WidgetStateProperty.all<EdgeInsets>(
                                              EdgeInsets.all(20)),
                                      backgroundColor:
                                          WidgetStateProperty.all<Color>(
                                              Colors.black),
                                      shape: WidgetStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      )))),
                              SizedBox(height: 20),
                            ],
                          )))),
            ],
          ),
        ));
  }
}
