// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'helper.dart';
import 'login.dart';
import 'nuevo_proyecto.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class Proyectos extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<Proyectos> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController statusController = TextEditingController();
  String ruta = "${server}proyectos";
  late List data;
  List dataId = [];

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(
        Uri.parse(ruta + "?user.company=" + company + "&_sort=id:DESC"),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      data = resBody;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(resBody[0]);
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
  }

  @override
  void initState() {
    super.initState();
    Helper.navigateUser(context);
    getData(ruta);
    // if (!_aceptado) {
    //   _aviso();
    // }
  }

  _modalDetalle(data) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(children: [
              Text('Detalle Proyecto'),
            ]),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(data['titulo']),
                  ),
                  Container(
                    child: Text(data['cliente']),
                  ),
                  Container(
                    child: Text(data['direccion']),
                  ),
                  Divider(),
                  Container(
                    child: Text(data['descripcion']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  SizedBox(width: 10),
                  Expanded(
                    child: OutlinedButton(
                      onPressed: () {
                        Helper.launchURL(data['lat'], data['long']);
                      },
                      child: const Text('Ver ubicación'),
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                      child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  )),
                  SizedBox(width: 10),
                  // SizedBox(width: 40),
                  // SizedBox(height: 20),
                ],
              ),
            ]);
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Proyectos',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          )),
      actions: <Widget>[],
    );
  }

  _listaProyectos() {
    if (data == null) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
        ),
      );
    } else if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              title: Text(item['titulo'],
                  style: TextStyle(
                    color: item['terminado'] ? Colors.red : Colors.black,
                    fontSize: 16.0,
                  )),
              subtitle: Text(
                item['created_at'].substring(0, 10) +
                    " - " +
                    item['cliente'] +
                    (item['terminado'] ? " - terminado" : ""),
              ),
              trailing: ClipOval(
                child: Material(
                  color: Colors.grey[300], // button color
                  child: InkWell(
                    splashColor: Colors.yellow[800], // inkwell color
                    child: SizedBox(
                        width: 56, height: 56, child: Icon(Icons.info)),
                    onTap: () {
                      getDataId(ruta, item['id'].toString());
                    },
                  ),
                ),
              ));
        },
      );
    } else {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: DrawerAdmin(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevoProyecto()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: _listaProyectos(),
                ),
              )
            ],
          ),
        ));
  }
}
