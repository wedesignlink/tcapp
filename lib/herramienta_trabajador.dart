// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:full_screen_image/full_screen_image.dart';
import 'package:intl/intl.dart';
import 'package:tc_app/nueva_herramienta.dart';
import 'package:tc_app/tools/constants.dart';
import 'drawer_admin.dart';
import 'drawer_trabajador.dart';
import 'helper.dart';
import 'funciones.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class HerramientaTrabajador extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<HerramientaTrabajador>
    with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}relacion-herramientas";
  String trabajadores = "${server}users";
  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataUsers = [];
  late List data;
  late String? _dropdownValueUser;
  var rol;

  void getDataTrabajadores(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(
        Uri.parse(ruta + "?role.id=2&company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataUsers = resBody;
    });
  }

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var userid = await Helper.getPreferences('userid');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var filterUser = "&user.id=" + userid!;
    var fechaActual = DateFormat('yyyy-MM-dd').format(DateTime.now());
    var fechaSeleccionadaInicio = DateFormat('yyyy-MM-dd').format(finicio);
    var filterfInicio = fechaActual != fechaSeleccionadaInicio
        ? "&created_at_gt=" + fechaSeleccionadaInicio.toString()
        : "";
    var fechaSeleccionadaFin = DateFormat('yyyy-MM-dd').format(ffin);
    var filterfFin = fechaActual != fechaSeleccionadaFin
        ? "&created_at_lt=" + fechaSeleccionadaFin.toString()
        : "";

    var response = await http.get(
        Uri.parse(ruta +
            "?user.company=" +
            company +
            filterUser +
            filterfInicio +
            filterfFin +
            "&_sort=created_at:DESC"),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      data = resBody;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(resBody[0]);
  }

  void _actualizarEntrega(ruta, id, entrega) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"Entregada": entrega};

    var response = await http.put(Uri.parse(ruta + "/" + id),
        body: json.encode(values), headers: headers);

    // final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      onSubmitted("Ok, La herramienta ha sido recibida");
      getData(ruta);
      Navigator.pop(context);
    } else {
      onSubmitted("Error!, No se ha podido recibir la herramienta");
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != finicio)
      setState(() {
        finicio = pickedDate;
      });
    getData(ruta);
  }

  Future<void> _selectDateFin(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != ffin)
      setState(() {
        ffin = pickedDate;
        // String finicio =
        //     "${pickedDate.year}/${pickedDate.month}/${pickedDate.day}";
      });
    getData(ruta);
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  @override
  void initState() {
    super.initState();
    // getDataTrabajadores(trabajadores);
    getData(ruta);
    getRol();
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    setState(() {
      rol = role;
    });
  }

  _limpiarFiltros(context) {
    setState(() {
      _dropdownValueUser = null;
      finicio = DateTime.now();
      ffin = DateTime.now();
    });
    getData(ruta);
    Navigator.pop(context);
  }

  _mostrarMenu() {
    if (rol == "34") {
      return DrawerAdmin();
    } else {
      return DrawerTrabajador();
    }
  }

  Widget _entregar(entregada, id) {
    if (entregada == "true") {
      return Container();
    } else if (entregada == null || entregada == "false") {
      if (rol == "34") {
        return ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(Colors.green),
          ),
          onPressed: () {
            // _modalDetalle();
            _actualizarEntrega(ruta, id, "true");
          },
          child: const Text('Recibir'),
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }

  Widget _btnEntregar(entregada, id) {
    if (entregada.toString() == "true") {
      return ElevatedButton(
        style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(Colors.green),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ))),
        onPressed: () {
          getDataId(ruta, id);
        },
        child: const Text('Entregada'),
      );
    } else if (entregada == null || entregada.toString() == "false") {
      return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(Colors.orange[400]!),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ))),
        onPressed: () {
          getDataId(ruta, id);
        },
        child: const Text('Pendiente'),
      );
    } else {
      return Container();
    }
  }

  Widget _imagenHerramienta(imagen) {
    if (imagen?.isEmpty ?? true) {
      // return Image.asset(
      //   'assets/not-image.png',
      //   fit: BoxFit.fitWidth,
      // );
      return Container();
    } else {
      return Image.network(
        baseUrl + imagen[0]['url'],
        fit: BoxFit.fitWidth,
      );
    }
  }

  _modalDetalle(data) {
    final horizontalPadding = 65;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            actionsPadding: EdgeInsets.only(
                right: MediaQuery.of(context).size.width / 2 -
                    horizontalPadding * 2),
            title: Column(children: [
              Text('Detalle herramienta'),
              FullScreenWidget(
                disposeLevel: DisposeLevel.High,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(16),
                  child: _imagenHerramienta(data['Imagen']),
                ),
              )
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(data['camionetas']['Nombre']),
                  ),
                  Center(
                    child: Text(data['herramienta']['Nombre']),
                  ),
                  Center(
                    child: Text("Fecha prestamo: " +
                        data['created_at'].toString().substring(0, 10)),
                  ),
                  Center(
                    child: Text(data['Entregada']
                        ? "Fecha entrega: " +
                            data['updated_at'].toString().substring(0, 10)
                        : "Pendiente de entrega"),
                  ),
                  Center(
                    child: Text(
                        data['user']['name'] + " " + data['user']['lastname']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  _entregar(
                      data['Entregada'].toString(), data['id'].toString()),
                  SizedBox(width: 20),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ]);
      },
    );
  }

  _modalFilter() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            // actionsPadding: EdgeInsets.only(
            //     right: MediaQuery.of(context).size.width / 2 -
            //         horizontalPadding * 2),
            title: Column(children: [
              Text('Filtrar'),
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      'Trabajador',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Container(
                      child: DropdownButtonFormField<String>(
                    isExpanded: true,
                    validator: (value) {
                      validateDrop(value!);
                      return null;
                    },
                    value: _dropdownValueUser,
                    hint: Text('Seleccione un trabajador'),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.black),
                    // underline: Container(
                    //   height: 2,
                    //   color: Colors.black,
                    // ),
                    onChanged: (newValue) async {
                      setState(() {
                        _dropdownValueUser = newValue!;
                      });
                      getData(ruta);
                    },
                    items: dataUsers.map((item) {
                      return new DropdownMenuItem(
                        child: Text(item['name'] + " " + item['lastname']),
                        value: item['id'].toString(),
                      );
                    }).toList(),
                  )),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      WidgetStateProperty.all<Color>(
                                          Colors.black),
                                  shape: WidgetStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ))),
                              onPressed: () => {_selectDate(context)},
                              child: Text('fecha Inicio'),
                            ),
                            _fechaMostrar(finicio)
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(20),
                        child: Column(
                          children: [
                            // SizedBox(
                            //   height: 20.0,
                            // ),
                            ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      WidgetStateProperty.all<Color>(
                                          Colors.black),
                                  shape: WidgetStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ))),
                              onPressed: () => {_selectDateFin(context)},
                              child: Text('fecha Fin'),
                            ),
                            _fechaMostrar(ffin),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.orange[400]!),
                    ),
                    onPressed: () {
                      _limpiarFiltros(context);
                    },
                    child: const Text('Limpiar'),
                  ),
                  SizedBox(width: 20),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ],
              ),
            ]);
      },
    );
  }

  Widget _fechaMostrar(fecha) {
    return Text(
        fecha.year.toString() +
            "/" +
            fecha.month.toString() +
            "/" +
            fecha.day.toString(),
        style: TextStyle(
          color: Color.fromARGB(255, 53, 66, 74),
          fontSize: 16.0,
          // fontWeight: FontWeight.bold,
        ));
  }

  Widget _btFiltrar() {
    if (rol == '34') {
      return IconButton(
          icon: const Icon(Icons.filter_alt),
          tooltip: 'Filtrar',
          onPressed: () {
            _modalFilter();
          });
    } else {
      return Container();
    }
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Herramientas',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[_btFiltrar()],
    );
  }

  Widget listaHerramienta() {
    if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
            title: Text(
                item['created_at'].toString().substring(0, 10) +
                    " - " +
                    item['herramienta']['Nombre'],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                )),
            subtitle: Text(
              item['user']['name'] +
                  " " +
                  item['user']['lastname'] +
                  "(" +
                  item['camionetas']['Placas'] +
                  ")",
            ),
            trailing: _btnEntregar(
                item['Entregada'].toString(), item['id'].toString()),
          );
        },
      );
    } else {
      return Center(
        child: Text('Sin datos para mostrar'),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: _mostrarMenu(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevaHerramienta()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: listaHerramienta(),
                ),
              )
            ],
          ),
        ));
  }
}
