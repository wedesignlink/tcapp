// ignore_for_file: unnecessary_null_comparison

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:tc_app/herramienta_trabajador.dart';
import 'package:tc_app/inicio_dashboard_trabajador.dart';
import 'package:tc_app/inicio_trabajador.dart';
import 'package:tc_app/mis_trabajos.dart';
import 'package:tc_app/recibos_trabajador.dart';
import 'login.dart';
// import 'herramienta.dart';

class DrawerTrabajador extends StatefulWidget {
  @override
  _MyDrawer createState() => new _MyDrawer();
}

class _MyDrawer extends State<DrawerTrabajador> {
  String _name = "";
  String _email = "";
  // String _rol = "";
  String _nivel = "";
  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  _getVars() async {
    var nombre = await getPreferences("username");
    var correo = await getPreferences("email");
    // var rol = await getPreferences("rolename");
    var nivel = await getPreferences("nivel");
    var nivelname;

    switch (nivel) {
      case "1":
        nivelname = "Supervisor";
        break;
      case "2":
        nivelname = "Maestro";
        break;
      case "3":
        nivelname = "Ayudante";
        break;
      default:
    }

    setState(() {
      _name = nombre!;
      _email = correo!;
      // _rol = rol;
      _nivel = nivelname;
    });
  }

  @override
  void initState() {
    super.initState();
    _getVars();
  }

  void logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs..clear();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Login()),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            // padding: EdgeInsets.only(left: 20.0),
            color: Colors.white,
            child: DrawerHeader(
                decoration: BoxDecoration(
                  border: Border.all(width: 0, color: Colors.white),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Image.asset('assets/user.png'),
                    Text(
                      _name != null ? _name : "",
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      _email != null ? _email : "",
                      style: TextStyle(
                        color: Color.fromARGB(255, 200, 200, 200),
                        fontSize: 10.0,
                      ),
                    ),
                    SizedBox(height: 5),
                    // Text(
                    //   _rol != null ? _rol : "",
                    //   style: TextStyle(
                    //     color: Color.fromARGB(255, 200, 200, 200),
                    //     fontSize: 10.0,
                    //   ),
                    // ),
                    Text(
                      _nivel != null ? _nivel : "",
                      style: TextStyle(
                        color: Color.fromARGB(255, 200, 200, 200),
                        fontSize: 10.0,
                      ),
                    ),
                  ],
                )),
          ),
          Container(
            padding: EdgeInsets.only(left: 20.0),
            color: Colors.white,
            height: screenHeight,
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_home.png'),
                  ),
                  title: Text(
                    'Escritorio',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => InicioDashboardTr()),
                    );
                  },
                ),
                const Divider(
                  color: Color.fromARGB(255, 244, 244, 244),
                  height: 20,
                  thickness: 1,
                  // indent: 20,
                  endIndent: 0,
                ),
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_plus.png'),
                  ),
                  title: Text(
                    'Iniciar trabajo',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => InicioTrabajador()),
                    );
                  },
                ),
                const Divider(
                  color: Color.fromARGB(255, 244, 244, 244),
                  height: 20,
                  thickness: 1,
                  // indent: 20,
                  endIndent: 0,
                ),
                // ListTile(
                //   leading: Container(
                //     child: Image.asset('assets/icon_home.png'),
                //   ),
                //   title: Text(
                //     'Iniciar trabajo',
                //     style: TextStyle(
                //       color: Color.fromARGB(255, 53, 66, 74),
                //       fontSize: 16.0,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                //   onTap: () {
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(builder: (context) => InicioAdmin()),
                //     );
                //   },
                // ),
                // const Divider(
                //   color: Color.fromARGB(255, 244, 244, 244),
                //   height: 20,
                //   thickness: 1,
                //   // indent: 20,
                //   endIndent: 0,
                // ),
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_reporte.png'),
                  ),
                  title: Text(
                    'Mis trabajos',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MisTrabajos()),
                    );
                  },
                ),
                const Divider(
                  color: Color.fromARGB(255, 244, 244, 244),
                  height: 20,
                  thickness: 1,
                  // indent: 20,
                  endIndent: 0,
                ),
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_herramientas.png'),
                  ),
                  title: Text(
                    'Herramientas',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HerramientaTrabajador()),
                    );
                  },
                ),
                const Divider(
                  color: Color.fromARGB(255, 244, 244, 244),
                  height: 20,
                  thickness: 1,
                  // indent: 20,
                  endIndent: 0,
                ),
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_recibos.png'),
                  ),
                  title: Text(
                    'Recibos',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RecibosTrabajador()),
                    );
                  },
                ),
                const Divider(
                  color: Color.fromARGB(255, 244, 244, 244),
                  height: 20,
                  thickness: 1,
                  // indent: 20,
                  endIndent: 0,
                ),
                // ListTile(
                //   leading: Container(
                //     child: Image.asset('assets/icon_shedule.png'),
                //   ),
                //   title: Text(
                //     'Shedule',
                //     style: TextStyle(
                //       color: Color.fromARGB(255, 53, 66, 74),
                //       fontSize: 16.0,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                //   onTap: () {
                //     // Update the state of the app.
                //     // ...
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(builder: (context) => Shedule()),
                //     );
                //   },
                // ),
                // const Divider(
                //   color: Color.fromARGB(255, 244, 244, 244),
                //   height: 20,
                //   thickness: 1,
                //   // indent: 20,
                //   endIndent: 0,
                // ),
                ListTile(
                  leading: Container(
                    child: Image.asset('assets/icon_exit.png'),
                  ),
                  title: Text(
                    'Cerrar sesión',
                    style: TextStyle(
                      color: Color.fromARGB(255, 53, 66, 74),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    logoutUser();
                  },
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
