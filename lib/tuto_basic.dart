// ignore_for_file: deprecated_member_use, duplicate_ignore

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
// // import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:tc_app/inicio_dashboard.dart';
import 'package:tc_app/tuto_inicio.dart';
import 'package:tc_app/tuto_location.dart';
import 'package:tc_app/tuto_recibos.dart';
import 'package:tc_app/tuto_trabajador.dart';
import 'package:tc_app/tuto_vehiculos.dart';
import 'drawer_trabajador.dart';
import 'helper.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class TutoBasic extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<TutoBasic> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}proyecto-detalles";

  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataProyectos = [];
  List dataUsers = [];
  List data = [];
  var rol;

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  void _actualizarAviso() async {
    var id = await Helper.getPreferences('userid');
    var token = await Helper.getPreferences('jwt');
    var ruta = "${server}users";
    var aviso = "true";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"aviso": aviso};

    print(values);

    var response = await http.put(Uri.parse(ruta + "/" + id!),
        body: json.encode(values), headers: headers);

    print(response.statusCode);

    if (response.statusCode == 200) {
      onSubmitted("Ok, El usuario se ha actualizado");
      await Helper.savePreferencesBool('aviso', true);

      Navigator.pop(context);
    } else {
      onSubmitted("Error!, El Usuario no se ha actualizado");
      Navigator.pop(context);
    }
  }

  void verificar() async {
    var aceptado = await Helper.getBoolPreferences('aviso');
    if (!aceptado!) {
      _aviso();
    }
    var tuto = await Helper.getBoolPreferences('tutorial');
    if (!tuto!) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => InicioDashboard()),
      );
    }
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    setState(() {
      rol = role;
    });
  }

  @override
  initState() {
    super.initState();
    verificar();
    getRol();
  }

  _aviso() async {
    WidgetsBinding.instance.addPostFrameCallback((_) => _modalPrivacidad());
  }

  _modalPrivacidad() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            title: new Text('Aviso de privacidad'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text('Debe aceptar el aviso de privacidad'),
                  // Linkify(
                  //   text: "Consultar el aviso https://cretezy.com",
                  //   options: LinkifyOptions(humanize: false),
                  // )
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              // ignore: deprecated_member_use
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                ),
                // padding: EdgeInsets.only(
                //     left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                // color: Colors.black,
                // shape: RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(25.0),
                //     side: BorderSide(color: Color.fromARGB(255, 254, 153, 2))),
                onPressed: () {
                  // Navigator.pop(context);
                  _actualizarAviso();
                  // _AceptarAviso();
                },
                child: Text(
                  'Acepto el aviso',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ]);
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Tutorial básico',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          )),
    );
  }

  _mostrarMenu() {
    if (rol == "34") {
      return DrawerAdmin();
    } else {
      return DrawerTrabajador();
    }
  }

  Widget dashboard() {
    return Center(
        child: Container(
      padding: EdgeInsets.only(left: 40.0, right: 40.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/logo_large.png"),
          fit: BoxFit.scaleDown,
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: _mostrarMenu(),
          // resizeToAvoidBottomInset: false,
          body: Container(
              // padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage("assets/fondo_tuto.png"),
                  fit: BoxFit.contain,
                  alignment: Alignment.bottomCenter,
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 40),
                    // titulo
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 0, bottom: 0),
                      child: Text(
                        'Tutorial básico',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 30.0,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    // texto
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 20, bottom: 0),
                      child: Text(
                        'Descubre otras funciones de tu app o concluye ahora el tutorial.',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 16.0,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    //vehiculos
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  Color.fromARGB(255, 186, 186, 186),
                              padding: EdgeInsets.all(15),
                            ),
                            // padding: EdgeInsets.only(
                            //     left: 20.0,
                            //     right: 20.0,
                            //     top: 20.0,
                            //     bottom: 20.0),
                            // color: Color.fromARGB(255, 186, 186, 186),
                            // elevation: 0.0,
                            // shape: RoundedRectangleBorder(
                            //   borderRadius: new BorderRadius.circular(30.0),
                            // ),
                            onPressed: () async {
                              // _validateInputs();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TutoVehiculos()),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Agregar vehículos y herramientas',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Montserrat',
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                )
                              ],
                            )),
                      ),
                    ),
                    // Recibos
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromARGB(255, 186, 186, 186),
                            padding: EdgeInsets.all(15),
                          ),
                          onPressed: () async {
                            // _validateInputs();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TutoRecibos()),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Agregar recibos de gastos',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Ubicacion
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromARGB(255, 186, 186, 186),
                            padding: EdgeInsets.all(15),
                          ),
                          onPressed: () async {
                            // _validateInputs();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TutoLocation()),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Ubicación de mis trabajadores',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  Color.fromARGB(255, 186, 186, 186),
                              padding: EdgeInsets.all(15),
                            ),
                            onPressed: () async {
                              // _validateInputs();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TutoInicio()),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Crear un proyecto',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Montserrat',
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                )
                              ],
                            )),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  Color.fromARGB(255, 186, 186, 186),
                              padding: EdgeInsets.all(15),
                            ),
                            onPressed: () async {
                              // _validateInputs();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TutoTrabajador()),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Crear un usuario',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Montserrat',
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                )
                              ],
                            )),
                      ),
                    ),

                    Row(
                      children: [
                        SizedBox(width: 40),
                        Expanded(
                            child: Container(
                          padding: EdgeInsets.only(top: 20),
                          child: SizedBox(
                            width: double.infinity,
                            // ignore: deprecated_member_use
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.yellow[700],
                              ),
                              // padding: EdgeInsets.only(
                              //     left: 20.0,
                              //     right: 20.0,
                              //     top: 20.0,
                              //     bottom: 20.0),
                              // color: Colors.yellow[700],
                              // elevation: 0.0,
                              // shape: RoundedRectangleBorder(
                              //   borderRadius: new BorderRadius.circular(30.0),
                              // ),
                              onPressed: () async {
                                Helper.actualizarPaso("1");
                                Helper.linkTuto("1", context);
                              },
                              child: Text(
                                'Reiniciar',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                            ),
                          ),
                        )),
                        SizedBox(width: 10),
                        Expanded(
                            child: Container(
                          padding: EdgeInsets.only(top: 20),
                          child: SizedBox(
                            width: double.infinity,
                            // ignore: deprecated_member_use
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.black),
                              // padding: EdgeInsets.only(
                              //     left: 20.0,
                              //     right: 20.0,
                              //     top: 20.0,
                              //     bottom: 20.0),
                              // color: Colors.black,
                              // elevation: 0.0,
                              // shape: RoundedRectangleBorder(
                              //   borderRadius: new BorderRadius.circular(30.0),
                              // ),
                              onPressed: () async {
                                Helper.terminarTuto(context);
                              },
                              child: Text(
                                'Terminar',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                            ),
                          ),
                        )),
                        SizedBox(width: 40),
                      ],
                    ),
                  ],
                ),
              ))),
    );
  }
}
