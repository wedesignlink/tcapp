String? validateName(String value) {
  if (value.length < 3)
    return 'El nombre debe contener más de 2 caracteres';
  else
    return null;
}

String? validateDir(String value) {
  if (value.length < 3)
    return 'La dirección debe contener más de 2 caracteres';
  else
    return null;
}

String? validateDesc(String value) {
  if (value.length < 3)
    return 'La descripción debe contener más de 2 caracteres';
  else
    return null;
}

String? validateNumber(String value) {
  if (value == "")
    return 'Debe ingresar un valor';
  else
    return null;
}

String? validateMobile(String value) {
  if (value.length != 10)
    return 'El número debe tener 10 dígitos';
  else
    return null;
}

String? validateDrop(String value) {
  return null;
}

String? validatePass(String value) {
// Indian Mobile number are of 10 digit only
  if (value.length < 8)
    return 'El password debe tener 8 caracteres.';
  else
    return null;
}

String? validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern.toString());
  if (!regex.hasMatch(value))
    return 'Escriba un email válido';
  else
    return null;
}
