import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tc_app/nuevo_shedule.dart';
import 'package:tc_app/tuto_vehiculos.dart';
import 'helper.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class Shedule extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<Shedule> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String ruta = "${server}Shedules";
  String rutap = "${server}proyectos";
  late List data;
  List dataId = [];
  bool _tuto = false;

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(
        Uri.parse(ruta +
            "?user_register.company=" +
            company +
            "&_sort=fecha_inicio:DESC"),
        headers: headers);

    var resBody = json.decode(response.body);
    var tutorial = await Helper.getBoolPreferences('tutorial') ?? false;
    // print(resBody);
    setState(() {
      data = resBody;
      _tuto = tutorial;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    print(resBody[0]);

    _modalDetalle(resBody[0]);
  }

  @override
  void initState() {
    super.initState();
    Helper.navigateUser(context);
    getData(ruta);
    // if (!_aceptado) {
    //   _aviso();
    // }
  }

  _modalDetalle(data) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(children: [
              Text('Detalle Shedule'),
            ]),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Divider(),
                  Container(
                    child: Text("Proyecto: " + data['proyecto']['titulo']),
                  ),
                  Container(
                    child: Text("Cliente: " + data['proyecto']['cliente']),
                  ),
                  Container(
                    child: Text("Dirección: " + data['proyecto']['direccion']),
                  ),
                  Container(
                    child:
                        Text("Descripción: " + data['proyecto']['descripcion']),
                  ),
                  Divider(),
                  Container(
                    child: Text("Asignado a: " +
                        data['user']['name'] +
                        " " +
                        data['user']['lastname']),
                  ),
                  Container(
                    child: Text("Fecha: " + data['fecha_inicio']),
                  ),
                  Container(
                    child: Text("Comentarios: " + data['descripcion']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  SizedBox(width: 10),
                  Expanded(
                    child: OutlinedButton(
                      onPressed: () {
                        Helper.launchURL(
                            data['proyecto']['lat'], data['proyecto']['long']);
                      },
                      child: const Text('Ver ubicación'),
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        // _guardaAlias();
                      },
                      child: Text(
                        'Cerrar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  // SizedBox(width: 40),
                  // SizedBox(height: 20),
                ],
              ),
            ]);
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Asignaciones',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          )),
      actions: <Widget>[],
    );
  }

  Widget _bottomBar(bool tuto) {
    if (tuto) {
      return Container(
        height: 170,
        padding: EdgeInsets.only(left: 40, right: 40, top: 20),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: 40, right: 40, top: 0, bottom: 10),
                    child: Text(
                      'Para agregar más asignaciones haz click en el botón +, cuando termines pulsa el botón "Siguiente paso"',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                    child: SizedBox(
                      width: double.infinity,
                      // ignore: deprecated_member_use
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.yellow[700],
                        ),
                        // padding: EdgeInsets.only(
                        //     left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                        // color: Colors.yellow[700],
                        // elevation: 0.0,
                        // shape: RoundedRectangleBorder(
                        //   borderRadius: new BorderRadius.circular(30.0),
                        // ),
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TutoVehiculos()),
                          );
                        },
                        child: Text(
                          'Siguiente paso',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  _listaAsiganciones() {
    if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              title: Text(
                  item['fecha_inicio'] +
                      " - " +
                      item['user']['name'] +
                      " " +
                      item['user']['lastname'],
                  style: TextStyle(
                    color: item['terminado'].toString() == "true"
                        ? Colors.red
                        : Colors.green,
                    fontSize: 16.0,
                  )),
              subtitle: Text(
                item['proyecto']['titulo'] +
                    (item['proyecto']['terminado'] ? " - terminado" : ""),
              ),
              trailing: ClipOval(
                child: Material(
                  color: Colors.grey[300], // button color
                  child: InkWell(
                    splashColor: Colors.yellow[800], // inkwell color
                    child: SizedBox(
                        width: 56, height: 56, child: Icon(Icons.info)),
                    onTap: () {
                      getDataId(ruta, item['id'].toString());
                    },
                  ),
                ),
              ));
        },
      );
    } else {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: deprecated_member_use
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: DrawerAdmin(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevoShedule()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                    padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: _listaAsiganciones()),
              ),
              _bottomBar(_tuto)
            ],
          ),
        ));
  }
}
