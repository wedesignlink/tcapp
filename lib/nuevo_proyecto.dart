// import 'package:donchonito_app/recibido.dart';

// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/funciones.dart';
import 'package:tc_app/proyectos.dart';
import 'package:tc_app/tools/constants.dart';
// import 'package:place_picker/place_picker.dart';
import 'package:tc_app/tuto_trabajador.dart';
import 'login.dart';
import 'helper.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevoProyecto extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevoProyecto> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController tituloController = TextEditingController();
  TextEditingController direccionController = TextEditingController();
  TextEditingController descripcionController = TextEditingController();
  TextEditingController clienteController = TextEditingController();
  TextEditingController precioController = TextEditingController();
  TextEditingController precioSController = TextEditingController();
  String ruta = "${server}proyectos";
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  late String _lat;
  late String _long;

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void guardarProyecto(ruta) async {
    var token = await getPreferences('jwt');
    var user = await getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "titulo": tituloController.text,
      "direccion": direccionController.text,
      "descripcion": descripcionController.text,
      "cliente": clienteController.text,
      "precio_pie": precioController.text,
      "precio_pie_supervisor": precioSController.text,
      "terminado": "false",
      "lat": _lat,
      "long": _long,
      "user": user!
    };

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    print(response.statusCode);
    print(responseData);

    if (response.statusCode == 200) {
      // setState(() => _loading = false);
      // _showDialog('OK!', 'Se guardado el usuario correctamente');
      cleanForm();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var tutorial = prefs.getBool('tutorial') ?? false;

      if (tutorial) {
        // Salvo el paso 1
        // Helper.savePreferences("paso", "1");
        Helper.actualizarPaso("1");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TutoTrabajador()),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Proyectos()),
        );
      }
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  // void showPlacePicker() async {
  //   LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
  //       builder: (context) =>
  //           PlacePicker("AIzaSyChhJkmyWpduakG29_I9fiN2rf7bwX0ts4")));
  //   // LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
  //   //     builder: (context) => PlacePicker(
  //   //         "AIzaSyChhJkmyWpduakG29_I9fiN2rf7bwX0ts4",
  //   //         displayLocation: customLocation)));

  //   // Handle the result in your way
  //   // final Location location = new Location(customLocation);
  //   var latitude = result.latLng.toString();
  //   latitude = latitude.substring(7);
  //   if (latitude != null && latitude.length > 0) {
  //     latitude = latitude.substring(0, latitude.length - 1);
  //   }
  //   var coords = latitude.split(",");

  //   print(coords[0]);
  //   print(result.formattedAddress);

  //   setState(() {
  //     direccionController.text = result.formattedAddress!;
  //     _lat = coords[0];
  //     _long = coords[1];
  //   });
  // }

  void cleanForm() {
    tituloController.clear();
    direccionController.clear();
    precioController.clear();
    precioSController.clear();
    descripcionController.clear();
    clienteController.clear();
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarProyecto(ruta);
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  // void getTutorial() async {
  //   var tutorial = Helper.getBoolPreferences('tutorial') ?? false;
  //   setState(() {
  //     _tutorial = tutorial;
  //   });

  //   print(tutorial);
  // }

  @override
  void initState() {
    super.initState();
    // getTutorial();
    // navigateUser();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Nuevo Proyecto',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[
              // IconButton(
              //   icon: const Icon(Icons.share),
              //   tooltip: 'Compartir',
              //   onPressed: () {
              //     // _modalAlias();
              //     // ScaffoldMessenger.of(context).showSnackBar(
              //     //     const SnackBar(content: Text('This is a snackbar')));
              //   },
              // ),
            ],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: tituloController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe un titulo'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: clienteController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe un cliente'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: descripcionController,
                                validator: (value) => validateDesc(value!),
                                maxLines: 4,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Escribe una descripción'),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              Divider(),
                              TextFormField(
                                controller: precioController,
                                validator: (value) => validateNumber(value!),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Precio por pie'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: precioSController,
                                validator: (value) => validateNumber(value!),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Precio por pie supervisor'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                enabled: false,
                                maxLines: 2,
                                controller: direccionController,
                                validator: (value) => validateDir(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Selecciona una dirección'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: Text('Ubicación',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: ElevatedButton(
                                onPressed: () {
                                  // showPlacePicker();
                                },
                                child:
                                    Icon(Icons.pin_drop, color: Colors.white),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(),
                                  padding: EdgeInsets.all(30),
                                  backgroundColor:
                                      Colors.yellow[700], // <-- Button color
                                ),
                              )),
                              SizedBox(
                                height: 40,
                              ),
                              ElevatedButton(
                                  child: Text('Guardar',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                      )),
                                  onPressed: () {
                                    _validateInputs();
                                  },
                                  style: ButtonStyle(
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.all(20)),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      )))),
                              SizedBox(
                                height: 40,
                              ),
                            ],
                          )))),
            ],
          ),
        ));
  }
}
