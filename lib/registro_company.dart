// ignore_for_file: deprecated_member_use

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:tc_app/login.dart';
import 'helper.dart';
import 'funciones.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class RegistroCompany extends StatefulWidget {
  final Map newUser;
  const RegistroCompany({Key? key, required this.newUser}) : super(key: key);
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<RegistroCompany> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}companies";
  String rutaRegister = "${server}auth/local/register";
  String rutaUpload = "${server}upload";
  TextEditingController companyController = TextEditingController();
  // TextEditingController montoController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  DateTime finicio = DateTime.now();
  late String? _dropdownValueMedidas;

  var _image;
  var imagePicker;
  var type;
  // var _path;
  // ImagePicker picker = ImagePicker();

  void guardarUsuario() async {
    // extraemos los datos del usuario
    Map<String, String> values = {
      "username": widget.newUser['name'],
      "name": widget.newUser['name'],
      "lastname": widget.newUser['lastname'],
      "email": widget.newUser['email'],
      "password": widget.newUser['password'],
      "confirmed": "1",
      "blocked": "0",
      "role": "34",
      "nivel": "1"
    };

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      // 'Authorization': 'Bearer $token'
    };

    http.Response response = await http.post(Uri.parse(rutaRegister),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      var token = responseData['jwt'];
      var user = responseData['user']['id'];

      // creamos la compañía
      GuardarCompany(token, user, responseData);
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  // ignore: non_constant_identifier_names
  void GuardarCompany(token, user, userData) async {
    Map<String, String> headersC = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map valuesC = {
      "nombre": companyController.text,
      "medidas": _dropdownValueMedidas,
      "users": [
        {"id": user.toString()}
      ],
      "admin": user.toString()
    };

    // print(json.encode(valuesC));

    http.Response responseC = await http.post(Uri.parse("${server}companies"),
        body: json.encode(valuesC), headers: headersC);

    final responseDataC = json.decode(responseC.body);

    // agregamos la imagen
    if (responseC.statusCode == 200) {
      // Helper.createSession(userData);
      // await Helper.savePreferences('company', responseDataC['id'].toString());
      // await Helper.savePreferences('companyName', responseDataC['nombre']);

      if (_image != null) {
        cargarImagen(_image, rutaUpload, responseDataC["id"], 'companies',
            'logo', token);
      }
      await Helper.savePreferencesBool('isRegister', true);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    } else {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseDataC["error"]);
    }
  }

  cargarImagen(image, rutaUpload, id, ref, field, token) async {
    print(image);
    print("ruta subir " + rutaUpload);
    print("id " + id.toString());
    print("ref " + ref);
    print("field " + field);
    print("token " + token);
    // var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token'
    };
    var uri = Uri.parse("${server}upload");

    var request = http.MultipartRequest('POST', uri)
      ..fields['ref'] = ref
      ..fields['refId'] = id.toString()
      ..fields['field'] = field
      ..files.add(new http.MultipartFile.fromBytes(
          'files', image.readAsBytesSync(),
          filename: "$field.${image.path.split(".").last}",
          contentType: MediaType("image", "${image.path.split(".").last}")));

    // print(image.path.split(".").last);
    print("$field.${image.path.split(".").last}");

    request.headers.addAll(headers);
    var response = await request.send();

    print(response.statusCode);
    print(response.reasonPhrase);
    print(response.request);

    if (response.statusCode == 200) print('Uploaded!');
  }

  uploadCamera() async {
    XFile image = await imagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      _image = File(image.path);
    });

    print(File(image.path));
  }

  uploadDevice() async {
    XFile image = await imagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(image.path);
    });

    print(File(image.path));
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarUsuario();
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();

    print(widget.newUser);
    // navigateUser();
  }

  // ignore: unused_element
  Widget _bloqueImagen() {
    if (_image == null) {
      return Container(
          child: ElevatedButton(
        onPressed: () {
          uploadDevice();
        },
        child: Icon(Icons.camera, color: Colors.white),
        style: ElevatedButton.styleFrom(
          shape: CircleBorder(),
          padding: EdgeInsets.all(30),
          backgroundColor: Colors.yellow[700], // <-- Button color
        ),
      ));
    } else {
      return Center(
          child: Image.file(
        _image,
        fit: BoxFit.fitWidth,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Registro',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                  child: Text('Empresa',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ))),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: companyController,
                                validator: (value) => validateName(value!),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Nombre de su empresa'),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Unidad de medida',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                isExpanded: true,
                                validator: (value) => validateDrop(value!),
                                value: _dropdownValueMedidas,
                                hint: Text('Seleccione una medida'),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _dropdownValueMedidas = newValue;
                                  });
                                },
                                items: <String>[
                                  'Pies',
                                  'Metros',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              )),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              // Center(
                              //     child: Text('Foto',
                              //         style: TextStyle(
                              //           color: Colors.black,
                              //           fontSize: 14.0,
                              //           // fontWeight: FontWeight.bold,
                              //         ))),
                              // SizedBox(
                              //   height: 10,
                              // ),
                              // _bloqueImagen(),
                              SizedBox(
                                height: 40,
                              ),
                              Container(
                                child: ElevatedButton(
                                    child: Text('Registrarme'),
                                    onPressed: () {
                                      _validateInputs();
                                    },
                                    style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(15)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.grey),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        )))),
                              )
                            ],
                          )))),
            ],
          ),
        ));
  }
}
