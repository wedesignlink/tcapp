// import 'package:donchonito_app/recibido.dart';

// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tc_app/herramienta.dart';
import 'funciones.dart';
import 'helper.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuevaHerramienta extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<NuevaHerramienta> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String users = "${server}users";
  String camionetas = "${server}camionetas";
  String herramientas = "${server}herramientas";
  String ruta = "${server}relacion-herramientas";
  List dataCamionetas = [];
  List dataHerramientas = [];
  List dataUsers = [];
  late String? _dropdownValueCamionetas;
  late String? _dropdownValueHerramientas;
  late String? _dropdownValueUsers;
  TextEditingController userController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  var rol;
  var username;

  void getDataCamionetas(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + '?user.company=' + company),
        headers: headers);

    var resBody = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        dataCamionetas = resBody;
      });
    }
  }

  void getDataUsers(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + '?company=' + company),
        headers: headers);

    var resBody = json.decode(response.body);

    if (response.statusCode == 200) {
      // if (_dropdownValueCamionetas != null) getDataHerramientas(herramientas);

      setState(() {
        dataUsers = resBody;
        // dataHerramientas = resBody['herramientas'];
      });
    }
  }

  void getDataHerramientas(ruta) async {
    var token = await Helper.getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(
        Uri.parse(
            ruta + "?camioneta.id=" + _dropdownValueCamionetas.toString()),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataHerramientas = resBody;
    });
    // print(dataNiveles);
  }

  void guardarSolicitud(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "camionetas": _dropdownValueCamionetas!,
      "herramienta": _dropdownValueHerramientas!,
      "user": rol == "34" ? _dropdownValueUsers! : user!,
    };

    http.Response response = await http.post(Uri.parse(ruta),
        body: json.encode(values), headers: headers);

    final responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      // setState(() => _loading = false);
      _showDialog('OK!', 'Se guardado el usuario correctamente');
      cleanForm();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Herramienta()),
      );
    } else if (response.statusCode == 400) {
      _showDialog(
          'Ups!',
          'No se ha podido guardar por favor intente de nuevo:\n\n' +
              responseData["error"]);
    }
  }

  void cleanForm() {
    _dropdownValueCamionetas = null;
    _dropdownValueHerramientas = null;
    _dropdownValueUsers = null;
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      // _formKey.currentState.save();
      guardarSolicitud(ruta);
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getDataCamionetas(camionetas);
    getDataUsers(users);
    getRol();
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    var usern = await Helper.getPreferences("username");
    setState(() {
      rol = role;
      userController.text = usern!;
    });
  }

  Widget listaHerramientas() {
    if (dataHerramientas.length > 0) {
      return Container(
          child: DropdownButtonFormField<String>(
        validator: (value) => validateDrop(value!),
        isExpanded: true,
        value: _dropdownValueHerramientas,
        hint: Text('Seleccione una herramienta'),
        // icon: const Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: const TextStyle(color: Colors.black),
        onChanged: (newValue) {
          setState(() {
            _dropdownValueHerramientas = newValue;
          });
        },
        items: dataHerramientas.map((item) {
          return new DropdownMenuItem(
            child: new Text(item['Nombre']),
            value: item['id'].toString(),
          );
        }).toList(),
      ));
    } else {
      return Container();
    }
  }

  Widget listaUsers() {
    if (rol == "34") {
      return Container(
          child: DropdownButtonFormField<String>(
        validator: (value) => validateDrop(value!),
        isExpanded: true,
        value: _dropdownValueUsers,
        hint: Text('Seleccione un usuario'),
        iconSize: 24,
        elevation: 16,
        style: const TextStyle(color: Colors.black),
        onChanged: (newValue) {
          setState(() {
            _dropdownValueUsers = newValue;
          });
        },
        items: dataUsers.map((item) {
          return new DropdownMenuItem(
            child: new Text(item['name'] + "" + item['lastname']),
            value: item['id'].toString(),
          );
        }).toList(),
      ));
    } else {
      return TextFormField(
        controller: userController,
        initialValue: username,
        enabled: false,
        decoration:
            InputDecoration(border: OutlineInputBorder(), hintText: 'Usuario'),
      );
    }
  }

  Widget _textoHerramienta() {
    if (rol == "34") {
      return Text('Asignar herramienta',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'Montserrat',
          ));
    } else {
      return Text('Recibir herramienta',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'Montserrat',
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    // String dropdownValueRole = '1';
    // String dropdownValue_nivel = 'Albañil';

    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Asignar Herramienta',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  // fontWeight: FontWeight.bold,
                )),
            actions: <Widget>[],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Form(
                          key: _formKey,
                          autovalidateMode: _autoValidate,
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                child: Text(
                                  'Usuario',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              listaUsers(),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Camionetas',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                  child: DropdownButtonFormField<String>(
                                isExpanded: true,
                                validator: (value) => validateDrop(value!),
                                value: _dropdownValueCamionetas,
                                hint: Text('Seleccione una camioneta'),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.black,
                                // ),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _dropdownValueCamionetas = newValue;
                                  });
                                  getDataHerramientas(herramientas);
                                },
                                items: dataCamionetas.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(item['Nombre']),
                                    value: item['id'].toString(),
                                  );
                                }).toList(),
                              )),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Text(
                                  'Herramienta',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 53, 66, 74),
                                    fontSize: 16.0,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              // Text(dataNiveles[0]['name']),
                              listaHerramientas(),
                              SizedBox(
                                height: 30,
                              ),

                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  child: _textoHerramienta(),
                                  onPressed: () {
                                    _validateInputs();
                                  },
                                  style: ButtonStyle(
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.all(20)),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      )))),
                              SizedBox(height: 20),
                            ],
                          )))),
            ],
          ),
        ));
  }
}
