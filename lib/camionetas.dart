// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/camionetas-herramientas.dart';
import 'package:tc_app/nueva_camioneta.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart' as seach;
import 'package:tc_app/tuto_basic.dart';
import 'helper.dart';
import 'login.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class Camionetas extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<Camionetas> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // late seach.SearchBar searchBar;
  String ruta = "${server}camionetas";
  List dataId = [];
  List data = [];
  bool _tuto = false;

  static Future<String?> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  void getData(ruta) async {
    var token = await getPreferences('jwt');
    var company = await getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?user.company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);
    // print(resBody);
    var tutorial = await Helper.getBoolPreferences('tutorial') ?? false;
    setState(() {
      data = resBody;
      _tuto = tutorial;
    });
  }

  void getDataId(ruta, id) async {
    var token = await getPreferences('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    // print(resBody[0]);

    _modalDetalle(resBody[0]);
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (!status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
  }

  @override
  void initState() {
    super.initState();
    getData(ruta);
  }

  _modalDetalle(data) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(children: [
              Text('Camioneta'),
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(data['Nombre']),
                  ),
                  Center(
                    child: Text(data['Placas']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  // OutlinedButton(
                  //   onPressed: () {
                  //     // _launchURL(data['lat'], data['long']);
                  //   },
                  //   child: const Text('Ir a la ubicación'),
                  // ),
                  SizedBox(width: 20),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                  // SizedBox(width: 40),
                  // SizedBox(height: 20),
                ],
              ),
            ]);
      },
    );
  }

  Widget _bottomBar(bool tuto) {
    if (tuto) {
      return Container(
        height: 170,
        padding: EdgeInsets.only(left: 40, right: 40, top: 20),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: 40, right: 40, top: 0, bottom: 10),
                    child: Text(
                      'Para agregar más vehículos haz click en el botón +, agrega herramienta en el botón lateral, cuando termines pulsa el botón "Siguiente paso"',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                    child: SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.yellow[700],
                          // backgroundColor: Colors.yellow[700],
                          // elevation: 0.0,
                          // shape: RoundedRectangleBorder(
                          //   borderRadius: new BorderRadius.circular(30.0),
                          // ),
                        ),
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TutoBasic()),
                          );
                        },
                        child: Text(
                          'Siguiente paso',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        centerTitle: true,
        title: Text('Vehículos',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16.0,
              // fontWeight: FontWeight.bold,
            )),
        actions: []
        // <Widget>[searchBar.getSearchAction(context)],
        );
  }

  // _MyStart() {
  //   searchBar = new seach.SearchBar(
  //       inBar: false,
  //       buildDefaultAppBar: buildAppBar,
  //       setState: setState,
  //       onSubmitted: onSubmitted,
  //       onCleared: () {
  //         print("cleared");
  //       },
  //       onClosed: () {
  //         print("closed");
  //         Navigator.push(
  //           context,
  //           MaterialPageRoute(builder: (context) => Camionetas()),
  //         );
  //       });
  // }

  _listaCamionetas() {
    if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              title: Text(item['Nombre'] + " (" + item['Placas'] + ")"),
              subtitle: Text(
                  (item['herramientas'].length.toString()) + " herramientas "),
              onTap: () {
                // _modalDetalle(data[0]);
                getDataId(ruta, item['id'].toString());
              },
              trailing: ClipOval(
                child: Material(
                  color: Colors.yellow[700], // button color
                  child: InkWell(
                    splashColor: Colors.yellow[800], // inkwell color
                    child: SizedBox(
                        width: 56,
                        height: 56,
                        child: Icon(Icons.plumbing_outlined)),
                    onTap: () {
                      // _modalDetalle();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CamionetasHerramientas(
                                id: item['id'].toString())),
                      );
                    },
                  ),
                ),
              ));
        },
      );
    } else {
      return Center(
        child: Text("Sin datos para mostrar"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          // appBar: searchBar.build(context),
          drawer: DrawerAdmin(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevaCamioneta()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: _listaCamionetas(),
                ),
              ),
              _bottomBar(_tuto),
            ],
          ),
        ));
  }
}
