// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:full_screen_image/full_screen_image.dart';
// // import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:csv/csv.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';
// import 'package:share_plus/share_plus.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tc_app/drawer_trabajador.dart';
import 'package:url_launcher/url_launcher.dart';
import 'nuevo_trabajo.dart';
import 'helper.dart';
import 'funciones.dart';
import 'drawer_admin.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class InicioTrabajador extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<InicioTrabajador> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}shedules";
  String trabajadores = "${server}users";
  String proyectos = "${server}proyectos";
  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataProyectos = [];
  List dataUsers = [];
  late List data;
  // ignore: unused_field
  var _totalPies = 0;
  // ignore: unused_field
  var _montoTotal = 0;
  late String? _dropdownValueUser;
  late String? _dropdownValueProy;
  late Position currentLocation;
  var rol;

  void getDataShedule(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    var fechaActual = DateFormat('yyyy-MM-dd').format(DateTime.now());

    var response = await http.get(
        Uri.parse(ruta +
            "?user.id=" +
            user.toString() +
            "&fecha_inicio=" +
            fechaActual +
            "&terminado=false"),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      data = resBody;
    });
  }

  void getDataProyectos(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await http.get(Uri.parse(ruta + "?user.company=" + company),
        headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      dataProyectos = resBody;
    });
  }

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var company = await Helper.getPreferences('company');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var filterUser =
        _dropdownValueUser != null ? "&user.id=" + _dropdownValueUser! : "";
    var filterProy =
        _dropdownValueProy != null ? "&proyecto.id=" + _dropdownValueProy! : "";
    var fechaActual = DateFormat('yyyy-MM-dd').format(DateTime.now());
    var fechaSeleccionadaInicio = DateFormat('yyyy-MM-dd').format(finicio);
    var filterfInicio = fechaActual != fechaSeleccionadaInicio
        ? "&created_at_gt=" + fechaSeleccionadaInicio.toString()
        : "";
    var fechaSeleccionadaFin = DateFormat('yyyy-MM-dd').format(ffin);
    var filterfFin = fechaActual != fechaSeleccionadaFin
        ? "&created_at_lt=" + fechaSeleccionadaFin.toString()
        : "";

    var response = await http.get(
        Uri.parse(ruta +
            "?user.company=" +
            company +
            filterUser +
            filterProy +
            filterfInicio +
            filterfFin +
            "&_sort=created_at:DESC"),
        headers: headers);

    var resBody = json.decode(response.body);

    var totalPies = 0;
    var montoTotal = 0;
    resBody.forEach((item) {
      //getting the key direectly from the name of the key
      totalPies += int.parse(item["Pies"]);
      montoTotal +=
          int.parse(item["Pies"]) * int.parse(item['proyecto']['precio_pie']);
    });

    setState(() {
      data = resBody;
      _totalPies = totalPies;
      _montoTotal = montoTotal;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(resBody[0]);
  }

  getCsv() async {
    //create an element rows of type list of list. All the above data set are stored in associate list
//Let associate be a model class with attributes name,gender and age and data be a list of associate model class.
    List<dynamic> row = [];

    List<List<dynamic>> rows = [];

    row.add("Nombre");
    row.add("Pies");
    row.add("Horas");
    row.add("Proyecto");
    row.add("Precio proyecto");
    row.add("Fecha");
    row.add("Comentarios");
    rows.add(row);

    for (int i = 0; i < data.length; i++) {
//row refer to each column of a row in csv file and rows refer to each row in a file
      row.add(data[i]['user']['name'] + " " + data[i]['user']['lastname']);
      row.add(data[i]['Pies']);
      row.add(data[i]['Horas']);
      row.add(data[i]['proyecto']['titulo']);
      row.add(data[i]['proyecto']['precio_pie']);
      row.add(data[i]['created_at']);
      row.add(data[i]['Comentarios']);
      rows.add(row);
    }

    //store file in documents folder
    // String dir =
    //     (await getExternalStorageDirectory()).absolute.path + "/documents";
    final dir = await getExternalStorageDirectory();

    var file = dir!.path + "/reporte.csv";
    File f = new File(file);

    // // convert rows to String and write as csv file

    String csv = const ListToCsvConverter().convert(rows);

    f.writeAsString(csv);

    // Share.shareFiles([f.path], text: 'Reporte');
  }

  Future<void> _selectDate(BuildContext context) async {
    // var formatter = new DateFormat('yyyy-MM-dd');
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != finicio)
      setState(() {
        finicio = pickedDate;
      });
    getData(ruta);
    Navigator.of(context).pop();
    _modalFilter(finicio, ffin);
  }

  Future<void> _selectDateFin(BuildContext context) async {
    // var formatter = new DateFormat('yyyy-MM-dd');
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != ffin)
      setState(() {
        ffin = pickedDate;
      });
    getData(ruta);
    Navigator.of(context).pop();
    _modalFilter(finicio, ffin);
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  void _actualizarAviso() async {
    var id = await Helper.getPreferences('userid');
    var token = await Helper.getPreferences('jwt');
    var ruta = "${server}users";
    var aviso = "true";

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {"aviso": aviso};

    var response = await http.put(Uri.parse(ruta + "/" + id!),
        body: json.encode(values), headers: headers);

    if (response.statusCode == 200) {
      onSubmitted("Ok, El usuario se ha actualizado");
      await Helper.savePreferencesBool('aviso', true);

      Navigator.pop(context);
    } else {
      onSubmitted("Error!, El Usuario no se ha actualizado");
      Navigator.pop(context);
    }
  }

  void verificar() async {
    var aceptado = await Helper.getBoolPreferences('aviso');
    if (!aceptado!) {
      _aviso();
    }
  }

  void getRol() async {
    var role = await Helper.getPreferences("roleid");
    setState(() {
      rol = role;
    });
  }

  @override
  initState() {
    super.initState();
    verificar();
    getDataShedule(ruta);
    // const oneSec = Duration(seconds: 120);
    // Timer.periodic(oneSec, (Timer t) => saveUserLocation());
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      Helper.showNewDialog(
          "Permisions", "Location services are disabled.", context);
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        Helper.showNewDialog(
            "Permisions", "Location permissions are denied", context);
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      Helper.showNewDialog(
          "Permisions",
          "Location permissions are permanently denied, we cannot request permissions.",
          context);
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  saveUserLocation() async {
    currentLocation = await _determinePosition();
    var token = await Helper.getPreferences('jwt');
    var userid = await Helper.getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    Map<String, String> values = {
      "lat": currentLocation.latitude.toString(),
      "long": currentLocation.longitude.toString()
    };

    var response = await http.put(Uri.parse(trabajadores + "/" + userid!),
        body: json.encode(values), headers: headers);

    if (response.statusCode == 200) {
      print('latitude ' + currentLocation.latitude.toString());
      print('long ' + currentLocation.longitude.toString());
      print("Ok, El usuario se ha actualizado");
    } else {
      print("Error!, El Usuario no se ha actualizado");
    }
  }

  _limpiarFiltros(context) {
    setState(() {
      _dropdownValueUser = null;
      finicio = DateTime.now();
      ffin = DateTime.now();
    });
    getData(ruta);
    Navigator.pop(context);
  }

  Widget _imagenHerramienta(imagen) {
    if (imagen?.isEmpty ?? true) {
      return Image.asset(
        'assets/not-image.png',
        fit: BoxFit.fitWidth,
      );
    } else {
      return Image.network(
        baseUrl + imagen['url'],
        fit: BoxFit.fitWidth,
      );
    }
  }

  _aviso() async {
    WidgetsBinding.instance.addPostFrameCallback((_) => _modalPrivacidad());
  }

  _modalPrivacidad() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            title: new Text('Aviso de privacidad'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text('Debe aceptar el aviso de privacidad'),
                  // Linkify(
                  //   text: "Consultar el aviso https://cretezy.com",
                  //   options: LinkifyOptions(humanize: false),
                  // )
                ],
              ),
            ),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                ),
                // padding: EdgeInsets.only(
                //     left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                // color: Colors.black,
                // shape: RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(25.0),
                //     side: BorderSide(color: Color.fromARGB(255, 254, 153, 2))),
                onPressed: () {
                  // Navigator.pop(context);
                  _actualizarAviso();
                  // _AceptarAviso();
                },
                child: Text(
                  'Acepto el aviso',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ]);
      },
    );
  }

  _modalDetalle(data) async {
    final horizontalPadding = 65;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            actionsPadding: EdgeInsets.only(
                right: MediaQuery.of(context).size.width / 2 -
                    horizontalPadding * 2),
            title: Column(children: [
              Text('Detalle recibo'),
              FullScreenWidget(
                disposeLevel: DisposeLevel.High,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(16),
                  child: _imagenHerramienta(data['Imagen']),
                ),
              )
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text("Fecha del recibo: " +
                        data['Fecha'].toString().substring(0, 10)),
                  ),
                  Center(
                    child: Text("Concepto: " + data['Concepto']),
                  ),
                  Center(
                    child: Text(
                        data['user']['name'] + " " + data['user']['lastname']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cancelar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ],
              ),
            ]);
      },
    );
  }

  Widget _fechaMostrar(fecha) {
    return Text(
        fecha.year.toString() +
            "/" +
            fecha.month.toString() +
            "/" +
            fecha.day.toString(),
        style: TextStyle(
          color: Color.fromARGB(255, 53, 66, 74),
          fontSize: 16.0,
          // fontWeight: FontWeight.bold,
        ));
  }

  _modalFilter(finicio, ffin) {
    // final horizontalPadding = 65;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
              title: Column(children: [
                Text('Filtrar'),
              ]),
              content: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        'Trabajador',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Container(
                        child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      validator: (value) => validateDrop(value!),
                      value: _dropdownValueUser,
                      hint: Text('Seleccione un trabajador'),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      // underline: Container(
                      //   height: 2,
                      //   color: Colors.black,
                      // ),
                      onChanged: (newValue) async {
                        setState(() {
                          _dropdownValueUser = newValue;
                        });
                        getData(ruta);
                      },
                      items: dataUsers.map((item) {
                        return new DropdownMenuItem(
                          child: Text(item['name'] + " " + item['lastname']),
                          value: item['id'].toString(),
                        );
                      }).toList(),
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        'Proyecto',
                        style: TextStyle(
                          color: Color.fromARGB(255, 53, 66, 74),
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Container(
                        child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      validator: (value) => validateDrop(value!),
                      value: _dropdownValueProy,
                      hint: Text('Seleccione un proyecto'),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      onChanged: (newValue) async {
                        setState(() {
                          _dropdownValueProy = newValue;
                        });
                        getData(ruta);
                      },
                      items: dataProyectos.map((item) {
                        return new DropdownMenuItem(
                          child: Text(item['titulo'] ?? ""),
                          value: item['id'].toString(),
                        );
                      }).toList(),
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.black),
                                    shape: WidgetStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ))),
                                onPressed: () => {_selectDate(context)},
                                child: Text('fecha de Inicio'),
                              ),
                              _fechaMostrar(finicio)
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              // SizedBox(
                              //   height: 20.0,
                              // ),
                              ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.black),
                                    shape: WidgetStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ))),
                                onPressed: () => {_selectDateFin(context)},
                                child: Text('fecha de Fin'),
                              ),
                              _fechaMostrar(ffin),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                Row(
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.orange[400]!),
                      ),
                      onPressed: () {
                        _limpiarFiltros(context);
                      },
                      child: const Text('Limpiar'),
                    ),
                    SizedBox(width: 20),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        // _guardaAlias();
                      },
                      child: Text(
                        'Cerrar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ],
                ),
              ]);
        });
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Iniciar trabajo',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[
        // IconButton(
        //   icon: const Icon(Icons.filter_alt),
        //   tooltip: 'Filtrar',
        //   onPressed: () {
        //     _modalFilter(finicio, ffin);
        //     // ScaffoldMessenger.of(context).showSnackBar(
        //     //     const SnackBar(content: Text('This is a snackbar')));
        //   },
        // ),
      ],
    );
  }

  _mostrarMenu() {
    if (rol == "34") {
      return DrawerAdmin();
    } else {
      return DrawerTrabajador();
    }
  }

  _botonElement(shedule, terminado) {
    if (!terminado) {
      return ClipOval(
          child: Material(
        color: Colors.grey[300], // button color
        child: InkWell(
          splashColor: Colors.yellow[800], // inkwell color
          child:
              SizedBox(width: 56, height: 56, child: Icon(Icons.start_rounded)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NuevoTrabajo(shedule: shedule)),
            );
          },
        ),
      ));
    } else {
      return ClipOval(
          child: Material(
        color: Colors.green[300], // button color
        child: InkWell(
          splashColor: Colors.yellow[800], // inkwell color
          child: SizedBox(width: 56, height: 56, child: Icon(Icons.check)),
          onTap: () {
            // getDataId(ruta, item['id'].toString());
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //       builder: (context) => NuevoTrabajo(shedule: shedule)),
            // );
          },
        ),
      ));
    }
  }

  Widget listaInicio() {
    if (data == null) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
        ),
      );
    } else if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
              onTap: () {
                Helper.launchURL(
                    item['proyecto']['lat'], item['proyecto']['long']);
              },
              title: Text(
                  item['fecha_inicio'].toString().substring(0, 10) +
                      "\n" +
                      item['proyecto']['titulo'] +
                      "\n" +
                      item['proyecto']['direccion'],
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                  )),
              subtitle: Text(
                item['user']['name'] + " " + item['user']['lastname'],
              ),
              trailing: _botonElement(item, item['terminado']));
        },
      );
    } else {
      return Center(child: Text('Sin trabajo programado'));
    }
  }

  // ignore: unused_element
  static void launchURL(lat, long) async {
    var url =
        'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + long;
    // if (await canLaunch(url)) {
    //   await launch(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
    if (!await launchUrl(
      Uri.parse(url),
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: _mostrarMenu(),
          // resizeToAvoidBottomInset: false,
          body: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: listaInicio(),
              )),
              // Container(
              //   height: 170,
              //   padding: EdgeInsets.only(left: 40, right: 40, top: 20),
              //   width: double.maxFinite,
              //   decoration: BoxDecoration(
              //       color: Colors.grey[200],
              //       borderRadius:
              //           BorderRadius.vertical(top: Radius.circular(20.0))),
              //   child: Row(
              //     mainAxisSize: MainAxisSize.max,
              //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //     children: <Widget>[
              //       Expanded(
              //         child: Column(
              //           children: [
              //             Text("Total de pies:" + _totalPies.toString()),
              //             Divider(color: Colors.black),
              //             Text(
              //               "\$" + _montoTotal.toString(),
              //               style: TextStyle(
              //                 color: Color.fromARGB(255, 53, 66, 74),
              //                 fontSize: 22.0,
              //                 fontWeight: FontWeight.bold,
              //               ),
              //             ),
              //             SizedBox(
              //               height: 5,
              //             ),
              //             Row(
              //               mainAxisSize: MainAxisSize.max,
              //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //               children: <Widget>[
              //                 IconButton(
              //                   icon: Icon(Icons.share),
              //                   onPressed: () {
              //                     getCsv();
              //                   },
              //                 ),
              //               ],
              //             )
              //           ],
              //         ),
              //       )
              //     ],
              //   ),
              // )
            ],
          ),
        ));
  }
}
