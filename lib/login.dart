// import 'package:donchonito_app/home_page.dart';
// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_app/inicio_dashboard_trabajador.dart';
import 'package:tc_app/registro.dart';
import 'funciones.dart';
import 'helper.dart';
import 'inicio_dashboard.dart';
import 'tools/constants.dart';

class Login extends StatefulWidget {
  @override
  _MyLoginState createState() => new _MyLoginState();
}

class _MyLoginState extends State<Login> {
  final _scaffoldkey = GlobalKey<ScaffoldState>();
  final String url = '${server}auth/local';
  // final String urlr =
  //     "https://donchonito.axo-corp.com/index.php/Services_Users/RecoveryPass";
  final String urlr =
      "http://www.devux.com.mx/dev/index.php/Login/PassRecoveryTC";
  late List data;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  late SharedPreferences prefs;
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  // bool _isRegister = false;

  TextEditingController emailRController = new TextEditingController();

  // user defined function
  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _modalRecuperar() {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            title: new Text('Recuperar contraseña'),
            content: TextFormField(
              controller: emailRController,
              autovalidateMode: _autoValidate,
              decoration: InputDecoration(
                  labelText: 'Escríbe tu email',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromARGB(255, 254, 153, 2)))),
            ),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromARGB(255, 254, 153, 2),
                ),
                // padding: EdgeInsets.only(
                //     left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                // color: Color.fromARGB(255, 254, 153, 2),
                // shape: RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(25.0),
                //     side: BorderSide(color: Color.fromARGB(255, 254, 153, 2))),
                onPressed: () {
                  _recuperarPass();
                },
                child: Text(
                  'Recuperar contraseña',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ]);
      },
    );
  }

  void _recuperarPass() async {
    var email = emailRController.text;
    var url = "${server}users?email=" + email;

    http.Response response = await http.get(Uri.parse(url));

    final responseData = json.decode(response.body);

    // print(response.statusCode);
    if (response.statusCode == 200) {
      Navigator.of(context).pop();
      // print(responseData[0]['id'].toString());
      var idUser = responseData[0]['id'].toString();

      var bodySend = {"email": email, "user": idUser};

      print(bodySend);
      var respuesta = await Helper.getSWData(urlr, bodySend: bodySend);
      var resBody = json.decode(respuesta.body);
      var data = resBody;

      print(data);

      if (data != null) {
        if (data[0]['code'] == '200') {
          _showDialog('Ok', data[0]['mensaje']);
          print(data[0]['pass']);
          Helper.savePreferences("temp_pass", data[0]['pass']);
          // print(data[0]['mensaje']);
        } else {
          _showDialog('Error', data[0]['mensaje']);
          // print(data[0]['mensaje']);
        }
      }
    }

    // Navigator.of(context).pop();

    // var bodySend = {"email": email};
    // var respuesta = await this.getSWData(urlr, bodySend: bodySend);
    // var resBody = json.decode(respuesta.body);
    // var data = resBody;

    // if (data[0]['code'] == '200') {
    //   _showDialog('Ok', data[0]['message']);
    // } else {
    //   _showDialog('Error', data[0]['message']);
    // }
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      _enviarDatos();
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  void _enviarDatos() async {
    if (emailController.text != "" && passwordController.text != "") {
      http.Response response = await http.post(Uri.parse('${server}auth/local'),
          body: {
            "identifier": emailController.text,
            "password": passwordController.text
          });

      final responseData = json.decode(response.body);

      print(response.statusCode);
      if (response.statusCode == 200) {
        if (responseData['user']['blocked']) {
          _showDialog('Ups!', 'El usuario no tiene permiso de acceso');
        } else {
          // guardamos las variables
          await Helper.savePreferences("jwt", responseData['jwt']);
          await Helper.savePreferences(
              "userid", responseData['user']['id'].toString());
          await Helper.savePreferences(
              "username", responseData['user']['username']);
          await Helper.savePreferences("email", responseData['user']['email']);
          await Helper.savePreferences(
              "roleid", responseData['user']['role']['id'].toString());
          await Helper.savePreferences(
              "nivel", responseData['user']['nivel']['id'].toString());
          await Helper.savePreferences(
              "precio", responseData['user']['price'].toString());
          await Helper.savePreferences(
              "rolename", responseData['user']['role']['name']);
          await Helper.savePreferencesBool('isLoggedIn', true);
          await Helper.savePreferencesBool(
              'aviso', responseData['user']['aviso'] ?? false);
          await Helper.savePreferencesBool(
              'tutorial', responseData['user']['tutorial'] ?? false);
          await Helper.savePreferences(
              'company', responseData['user']['company']['id'].toString());
          await Helper.savePreferences(
              'companyName', responseData['user']['company']['nombre']);
          await Helper.savePreferences(
              'paso',
              responseData['user']['paso'] != null
                  ? responseData['user']['paso']['id'].toString()
                  : "0");
          print("paso");
          print(responseData['user']['paso'] != null
              ? responseData['user']['paso']['id'].toString()
              : "0");
          // Helper.createSession(responseData);
          // await Helper.savePreferences(
          //     'company', responseData['user']['company']['id'].toString());
          // await Helper.savePreferences(
          //     'companyName', responseData['user']['company']['nombre']);

          //redirect to product
          if (responseData['user']['role']['id'] == 34) {
            if (responseData['user']['tutorial'] ?? false) {
              var paso = responseData['user']['paso'] != null
                  ? responseData['user']['paso']['id'].toString()
                  : "0";
              Helper.linkTuto(paso, context);
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => InicioDashboard()),
              );
            }
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => InicioDashboardTr()),
            );
          }
        }
      } else {
        _showDialog('Ups!', 'No fue posible acceder');
      }
    } else {
      _showDialog('Ups!', 'Debe llenar todos los campos');
    }
  }

  void _verificarLogin() async {
    var isRegister = await Helper.getBoolPreferences('isRegister');

    if (isRegister != null && isRegister == true) {
      _showDialog("Ok",
          "Se ha registrado correctamente, por favor ingrese con su usuario y contraseña.");
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs..clear();
    }
  }

  @override
  void initState() {
    super.initState();
    _verificarLogin();
    // _modalAlias();
    // _dropdown();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;

    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            key: _scaffoldkey,
            resizeToAvoidBottomInset: false,
            body: Container(
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 226, 170, 49),
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.fromLTRB(80.0,
                                    screenHeight / 8, 80.0, screenHeight / 12),
                                padding: EdgeInsets.fromLTRB(40.0,
                                    screenHeight / 8, 40.0, screenHeight / 20),
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 226, 170, 49),
                                  image: DecorationImage(
                                    image:
                                        AssetImage("assets/tc_logo_text.png"),
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 30.0, right: 30.0),
                          child: Card(
                            // elevation: 3.0,
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(60.0)),
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10.0, left: 20.0, right: 20.0),
                              decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.circular(60.0),
                                  color: Colors.transparent),
                              child: SizedBox(
                                height: 55.0,
                                child: TextFormField(
                                  controller: emailController,
                                  validator: (value) => validateEmail(value!),
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                      // labelText: 'Usuario',
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                      hintText: 'Usuario',
                                      labelStyle: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      border: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      errorStyle: TextStyle(
                                        // fontWeight: FontWeight.bold,
                                        height: .05,
                                      )),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 30.0, right: 30.0),
                          child: Card(
                            // elevation: 3.0,
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(60.0)),
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10.0, left: 20.0, right: 20.0),
                              decoration: BoxDecoration(
                                // color: Colors.white,
                                borderRadius: BorderRadius.circular(60.0),
                              ),
                              child: SizedBox(
                                height: 55.0,
                                child: TextFormField(
                                  controller: passwordController,
                                  validator: (value) => validatePass(value!),
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                      // labelText: 'Contraseña',
                                      hintText: 'Contraseña',
                                      labelStyle: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      border: UnderlineInputBorder(
                                          borderSide: BorderSide.none),
                                      errorStyle: TextStyle(
                                        // fontWeight: FontWeight.bold,
                                        height: .05,
                                      )),
                                  obscureText: true,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 35.0, left: 40.0, right: 40.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: SizedBox(
                                  width: double.infinity,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.black,
                                        padding: EdgeInsets.all(20),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30))),
                                    onPressed: () async {
                                      _validateInputs();
                                    },
                                    child: Text(
                                      'Iniciar Sesión',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                child: SizedBox(
                                  width: double.infinity,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.yellow[700],
                                        padding: EdgeInsets.all(20),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30))),
                                    onPressed: () async {
                                      // _validateInputs();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Registro()),
                                      );
                                    },
                                    child: Text(
                                      'Registrarme',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Recuperar contrasena.',
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white,
                                )),
                            SizedBox(
                              width: 10.0,
                            ),
                            InkWell(
                              onTap: () {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => Register()),
                                // );
                                _modalRecuperar();
                              },
                              child: Text(
                                'Recuperar',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Monsterrat',
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                      ]),
                ))));
  }
}
