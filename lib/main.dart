import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:page_transition/page_transition.dart';

import 'login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late SharedPreferences prefs;
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    // print(status);
    if (status) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Time Consolidation',
        theme: ThemeData(
          primarySwatch: Colors.orange,
          textTheme: TextTheme(
            labelLarge: TextStyle(color: Colors.white),
          ),
        ),
        // darkTheme: ThemeData(
        //   brightness: Brightness.light,
        //   primaryColor: Colors.black,
        //   /* dark theme settings */
        // ),
        themeMode: ThemeMode.light,
        // navigatorKey: navigatorKey,
        // initialRoute: 'login',
        // routes: {'login': (BuildContext context) => Login()},
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: 'assets/tc_logo.png',
            nextScreen: Login(),
            splashTransition: SplashTransition.fadeTransition,
            pageTransitionType: PageTransitionType.bottomToTop,
            backgroundColor: Color.fromARGB(255, 226, 170, 49)));
  }
}
