// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:full_screen_image/full_screen_image.dart';
import 'package:tc_app/drawer_trabajador.dart';
import 'helper.dart';
import 'funciones.dart';
import 'nuevo_recibo.dart';
import 'tools/constants.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class RecibosTrabajador extends StatefulWidget {
  @override
  _MyStart createState() => new _MyStart();
}

class _MyStart extends State<RecibosTrabajador> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String ruta = "${server}recibos";
  String trabajadores = "${server}users";
  String baseUrl = "http://159.203.164.232";
  DateTime finicio = DateTime.now();
  DateTime ffin = DateTime.now();
  List dataUsers = [];
  late List data;
  late String? _dropdownValueUser;

  void getData(ruta) async {
    var token = await Helper.getPreferences('jwt');
    var user = await Helper.getPreferences('userid');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var filterUser = "user.id=" + user!;
    var rutaR = ruta + "?" + filterUser + "&_sort=id:DESC";

    var response = await http.get(Uri.parse(rutaR), headers: headers);

    var resBody = json.decode(response.body);

    setState(() {
      data = resBody;
    });
  }

  void getDataId(ruta, id) async {
    var token = await Helper.getPreferences('jwt');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response =
        await http.get(Uri.parse(ruta + "?id=" + id), headers: headers);
    var resBody = json.decode(response.body);

    _modalDetalle(resBody[0]);
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != finicio)
      setState(() {
        finicio = pickedDate;
      });
    getData(ruta);
    Navigator.of(context).pop();
    _modalFilter();
  }

  Future<void> _selectDateFin(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: finicio,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != ffin)
      setState(() {
        ffin = pickedDate;
      });
    getData(ruta);
    Navigator.of(context).pop();
    _modalFilter();
  }

  void onSubmitted(String value) {
    setState(() => ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: new Text('$value!'))));
  }

  @override
  void initState() {
    super.initState();
    // getDataTrabajadores(trabajadores);
    getData(ruta);
    // navigateUser();
    // if (!_aceptado) {
    //   _aviso();
    // }
  }

  _limpiarFiltros(context) {
    setState(() {
      _dropdownValueUser = null;
      finicio = DateTime.now();
      ffin = DateTime.now();
    });
    getData(ruta);
    Navigator.pop(context);
  }

  Widget _imagenHerramienta(imagen) {
    if (imagen?.isEmpty ?? true) {
      return Image.asset(
        'assets/not-image.png',
        fit: BoxFit.fitWidth,
      );
    } else {
      return Image.network(
        baseUrl + imagen['url'],
        fit: BoxFit.fitWidth,
      );
    }
  }

  _modalDetalle(data) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(children: [
              Text('Detalle recibo'),
              FullScreenWidget(
                disposeLevel: DisposeLevel.High,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(16),
                  child: _imagenHerramienta(data['Imagen']),
                ),
              )
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text("Fecha del recibo: " +
                        data['Fecha'].toString().substring(0, 10)),
                  ),
                  Center(
                    child: Text("Concepto: " + data['Concepto']),
                  ),
                  Center(
                    child: Text(
                        data['user']['name'] + " " + data['user']['lastname']),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        // _guardaAlias();
                      },
                      child: Text(
                        'Cancelar',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  )
                ],
              )
            ]);
      },
    );
  }

  _modalFilter() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            // actionsPadding: EdgeInsets.only(
            //     right: MediaQuery.of(context).size.width / 2 -
            //         horizontalPadding * 2),
            title: Column(children: [
              Text('Filtrar'),
            ]),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      'Trabajador',
                      style: TextStyle(
                        color: Color.fromARGB(255, 53, 66, 74),
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Container(
                      child: DropdownButtonFormField<String>(
                    isExpanded: true,
                    validator: (value) => validateDrop(value!),
                    value: _dropdownValueUser,
                    hint: Text('Seleccione un trabajador'),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.black),
                    // underline: Container(
                    //   height: 2,
                    //   color: Colors.black,
                    // ),
                    onChanged: (newValue) async {
                      setState(() {
                        _dropdownValueUser = newValue;
                      });
                      getData(ruta);
                    },
                    items: dataUsers.map((item) {
                      return new DropdownMenuItem(
                        child: Text(item['name'] + " " + item['lastname']),
                        value: item['id'].toString(),
                      );
                    }).toList(),
                  )),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      WidgetStateProperty.all<Color>(
                                          Colors.black),
                                  shape: WidgetStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ))),
                              onPressed: () => {_selectDate(context)},
                              child: Text('fecha de Inicio'),
                            ),
                            Text(
                                finicio.year.toString() +
                                    "/" +
                                    finicio.month.toString() +
                                    "/" +
                                    finicio.day.toString(),
                                style: TextStyle(
                                  color: Color.fromARGB(255, 53, 66, 74),
                                  fontSize: 16.0,
                                  // fontWeight: FontWeight.bold,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            // SizedBox(
                            //   height: 20.0,
                            // ),
                            ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      WidgetStateProperty.all<Color>(
                                          Colors.black),
                                  shape: WidgetStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ))),
                              onPressed: () => {_selectDateFin(context)},
                              child: Text('fecha de Fin'),
                            ),
                            Text(
                                ffin.year.toString() +
                                    "/" +
                                    ffin.month.toString() +
                                    "/" +
                                    ffin.day.toString(),
                                style: TextStyle(
                                  color: Color.fromARGB(255, 53, 66, 74),
                                  fontSize: 16.0,
                                  // fontWeight: FontWeight.bold,
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Row(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.orange[400]!),
                    ),
                    onPressed: () {
                      _limpiarFiltros(context);
                    },
                    child: const Text('Limpiar'),
                  ),
                  SizedBox(width: 20),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.black),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      // _guardaAlias();
                    },
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ],
              ),
            ]);
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: Text('Recibos',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            // fontWeight: FontWeight.bold,
          )),
      actions: <Widget>[
        // searchBar.getSearchAction(context)
        // IconButton(
        //   icon: const Icon(Icons.filter_alt),
        //   tooltip: 'Filtrar',
        //   onPressed: () {
        //     _modalFilter();
        //   },
        // ),
      ],
    );
  }

  Widget listaRecibos() {
    if (data == null) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
        ),
      );
    } else if (data.length > 0)
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return ListTile(
            title: Text(item['Monto'].toString() + " " + item['Concepto'],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                )),
            subtitle: Text(item['Fecha'].toString() != null
                ? item['Fecha'].toString().substring(0, 10)
                : ""),
            trailing: ClipOval(
              child: Material(
                color: Colors.grey[300], // button color
                child: InkWell(
                  splashColor: Colors.yellow[800], // inkwell color
                  child:
                      SizedBox(width: 56, height: 56, child: Icon(Icons.info)),
                  onTap: () {
                    getDataId(ruta, item['id'].toString());
                  },
                ),
              ),
            ),
          );
        },
      );
    else
      return Center(
        child: Text("Sin datos para mostrar"),
      );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(context),
          drawer: DrawerTrabajador(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NuevoRecibo()),
              );
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.yellow[700],
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                    padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: listaRecibos()),
              )
            ],
          ),
        ));
  }
}
